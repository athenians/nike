#########################################
#
#streamline_2d returns an array of x,y coordinates from a dictionary containing x1,x2,v1,v2 and 
#the start point, which should be in cartesian coordinates x,y
#rmax is an optional value which defines the outer edge that we want to trak point to.
#tdisk is also optional, it defined where the disk starts, and if a streamline ends up here we truncate it.
#
#   v1 is the radial velocity
#   v2 is the velocity in the theta direction
#   x1 is theta
#   x2 is r
#   start is a 2 component array with the w,z starting point for the streamline
#   coord_sys at the moment only accepts spol. If any other systems are required, this will need coding up
#   rmax is an optional argument, it is the outer radius to which we want to track the streamline
#   tdisk is the grid cell number which defines the surface of the disk. -2 is the penultimate cell in the theta array
#   max_steps is the number of points we will track the streamline for before abandoning it.
#
#######################################
import os
import numpy as np
import cPickle as pickle

def fieldline_2d(i_dump,dump_dir,grid_dic,v1,v2,x1,x2,start,coord_sys,rmax=-1,tdisk=-2,max_steps=10000,RECALC=0):

	streamline_name = 'r_' + `start[0]`
	streamline_dir = dump_dir + 'all_fieldlines/' + 'dump' + str(i_dump).zfill(5) + '/'
	streamfile = streamline_dir + streamline_name
	
	try: # OPEN PREVIOUSLY COMPUTED STREAMLINE
		Fieldline = pickle.load(open(streamfile + '.p', "rb"))
	
	except: # CALCULATE THE STREAMLINE COORDS
		Fieldline = calc_streamline_2d(v1,v2,x1,x2,grid_dic['lengths'],start,coord_sys,rmax=rmax,tdisk=tdisk,max_steps=max_steps,sim_dic=None)
		if not os.path.exists(streamline_dir):
			os.makedirs(streamline_dir)
		pickle.dump(Fieldline, open(streamfile + '.p', "wb"), protocol=2)

	finally: # FORCE A REDO CALC 
		if RECALC:
			Fieldline = calc_streamline_2d(v1,v2,x1,x2,grid_dic['lengths'],start,coord_sys,rmax=rmax,tdisk=tdisk,max_steps=max_steps,sim_dic=None)
			pickle.dump(Fieldline, open(streamfile + '.p', "wb"), protocol=2)
			
	return Fieldline

def streamline_2d(i_dump,dump_dir,grid_dic,v1,v2,x1,x2,start,coord_sys,rmax=-1,tdisk=-2,max_steps=10000,RECALC=0,sim_dic=None):

	streamline_name = 'r_' + `start[0]`
	streamline_dir = dump_dir + 'all_streamlines/' + 'dump' + str(i_dump).zfill(5) + '/'
	streamfile = streamline_dir + streamline_name
	
	try: # OPEN PREVIOUSLY COMPUTED STREAMLINE
		Streamline = pickle.load(open(streamfile + '.p', "rb"))
	
	except: # CALCULATE THE STREAMLINE COORDS
		Streamline = calc_streamline_2d(v1,v2,x1,x2,grid_dic['lengths'],start,coord_sys,rmax=rmax,tdisk=tdisk,max_steps=max_steps,sim_dic=sim_dic)
		if not os.path.exists(streamline_dir):
			os.makedirs(streamline_dir)
		pickle.dump(Streamline, open(streamfile + '.p', "wb"), protocol=2)

	finally: # FORCE A REDO CALC 
		if RECALC:
			Streamline = calc_streamline_2d(v1,v2,x1,x2,grid_dic['lengths'],start,coord_sys,rmax=rmax,tdisk=tdisk,max_steps=max_steps,sim_dic=sim_dic)
			pickle.dump(Streamline, open(streamfile + '.p', "wb"), protocol=2)
			
	return Streamline
	
def calc_streamline_2d(v1,v2,x1,x2,lengths,start,coord_sys,rmax,tdisk,max_steps,sim_dic):

	#At the moment, this only works for spol coordinates. 
	if coord_sys!='spol':
		print "Error: Streamline 2d - unknown coordinate type ",coord_sys
	
	# Store all results in this dictionary
	Streamline = {'r_base':start[0],'theta_base':start[1]}

	# Set up two arrays to hold the r, theta coordinates of the streamline
	r=[]
	theta=[]

	Ntheta = len(x1) 

	# Optional - gather all hydro/mhd variables along the streamline
	gather_hydro_data = False
	if sim_dic is not None:
		gather_hydro_data = True 
		for key in sim_dic.keys():
			Streamline[key] = []
		print "[calc_streamline_2d]: will gather the following quantities along streamline-\n".format(Streamline.keys())


	# If rmax is not supplied, or is greater than the maximum radius in the simulation, set it to just inside the outer radius.
	if rmax==-1 or rmax>=max(x2):
		rmax=max(x2)*0.999

	#First, compute the theta coordinate of the start of the streamline
	#start[1] is the y coordinate. We need to deal with the situation where a user asks for y=0, this will give an infinity 
	if start[1]==0.0:
		tstart=np.pi/2.0   #Deal with potential infinity
	else:
		tstart=np.arctan(start[0]/start[1])
       

	#Check whether the user has sent a root location that is inside the disk. This is, by default, 2 cells from the end
	if tstart>x1[tdisk]:
		tstart=x1[tdisk]*0.9999999
		print "Warning: Streamline_2d - requested start point in disk - resetting to just above disk"

	
	#Append the root location as the first point in the theta array
	theta.append(tstart)

	#Now compute the starting radius
	rstart=np.sqrt(start[1]**2+start[0]**2)\

	#Check that we are in the computational domain
	if rstart<x2[0]:
		rstart=x2[0]*1.00001
	elif rstart>rmax:
		rstart=rmax

	#Append the root location as the first point in the r array
	r.append(rstart)

	push=1.01   #This is a variable to try to ensure we dont end up exactly on a boundary
	status="tracking" #This flag is set to tracking while we are following a streamline. Any condition that means we want to stop following should result in this flag being changed to something else, and the loop being exited at the end.
	i=0
	while status=="tracking":	
		#Find out which cell we are in - 
		j,k,frac1,frac2=get_index(x1,x2,theta[-1],r[-1])

		#Compute the two velocity components by interpolation 
		tempv1=interpolate(j,k,frac1,frac2,v1,Ntheta)
		tempv2=interpolate(j,k,frac1,frac2,v2,Ntheta)

		# gather any desired hydro data along streamline by interpolation
		if gather_hydro_data:	
			for key in sim_dic.keys():
				Streamline[key].append(interpolate(j,k,frac1,frac2,sim_dic[key],Ntheta))

		dtheta = np.arctan(tempv2/r[-1])

		#We step across a cell my moving lengths given by the velocity x a time. This next line computes a time step for
		#the current cell, simply by taking the smallest length, and dividing by the fastest velocity, and then by 10.	
		dt=(lengths[j][k]/np.max(np.abs([tempv1,tempv2])))/10.0

		#Compute the new theta and r coordinates, just multiply the relevant velocities by the time step.
		#We will check that these two 
		tnew=theta[-1]+(dtheta*dt)
		rnew=r[-1]+tempv1*dt


		#We are now going to do a little testing. We want to make sure that if we enter a new cell, we dont jump too 
		#far across that new cell.
		#We begin by setting two new times, that we will use in a minute. Set the to the age of the universe as default
		#NB, this was the age of the universe (to 1SF) as of 23rd March 2015. 
		r_time=4e17
		theta_time=4e17

		#The following tests see if we will go up or down a cell in r or theta, and sets a time that will just about take us
		#into the new cell. This means we won't race across a cell
		if tnew < x1[j] and theta[-1] != x1[j] :
			theta_time=push*(abs((x1[j]-theta[-1])/(dtheta)))
		if tnew > x1[j+1] and theta[-1] != x1[j+1]:
			theta_time=push*(abs((x1[j+1]-theta[-1])/(dtheta)))
		if rnew > x2[k+1]:
			r_time=push*(abs((x2[k+1]-r[-1])/tempv1))
		if rnew < x2[k]:
			r_time=push*(abs((x2[k]-r[-1])/tempv1))

		#Compute new, new values of r and theta
		tnew=theta[-1]+(dtheta*min(dt,r_time,theta_time))
		rnew=r[-1]+tempv1*min(dt,r_time,theta_time)

		#Now we tests to see if we have reached any of the limits that would cause us to stop tracking the streamline
		#The message that the code outputs is pretty self explanatory 
		if tnew > x1[tdisk] :
			print "At upper theta limit (hit disk) after ",i," steps"
			status="hit_disk"
		elif tnew < x1[0] :
			print "At lower theta limit after ",i," steps"	
			status="hit_pole"
		elif rnew > rmax: 
			print "At outer radial boundary after ",i," steps"
			status="reached_outer"
		elif rnew < x2[0]:
			print "At inner radial boundary after ",i," steps"
			status="reached_inner"
		elif tnew==theta[-1] and rnew==r[-1]:
			print "Stalled after ",i," steps"
			status="stalled"
		elif i>max_steps:
			print "Tracked for ",i," steps, aborting"
			status="too_many_steps"
		else:	
		#If there are no stop conditions, append the new r and theta coordinates onto the arrays
			r.append(rnew)
			theta.append(tnew)
			i=i+1

	#We want to return cartesian coordinates so loop over the r,theta coordinates and turn them back.
	theta = np.array(theta)
	r = np.array(r)
	
	Streamline['x'] = r*np.sin(theta)
	Streamline['z'] = r*np.cos(theta)
	Streamline['status'] = status
	Streamline['Nsteps'] = i
	if gather_hydro_data:
		for key in sim_dic.keys():
			Streamline[key] = np.array(Streamline[key])
	
	return Streamline 

def test_streamline_2d(v1,v2,x1,x2,lengths,test_coords,coord_sys,rmax,tdisk,max_steps,sim_dic):

	#At the moment, this only works for spol coordinates. 
	if coord_sys!='spol':
		print "Error: Streamline 2d - unknown coordinate type ",coord_sys
	
	# Set up two arrays to hold the r, theta coordinates of the streamline
	r=np.linspace(test_coords[0],max(x2)*0.999,500)
	theta=np.ones_like(r)*test_coords[1]
	
	# Store all results in this dictionary
	Streamline = {'r_base':r[0],'theta_base':theta[0]}

	Ntheta = len(x1) 

	# Optional - gather all hydro/mhd variables along the streamline
	gather_hydro_data = False
	if sim_dic is not None:
		gather_hydro_data = True 
		for key in sim_dic.keys():
			Streamline[key] = []
		print "[calc_streamline_2d]: will gather the following quantities along streamline-\n".format(Streamline.keys())


	# If rmax is not supplied, or is greater than the maximum radius in the simulation, set it to just inside the outer radius.
	if rmax==-1 or rmax>=max(x2):
		rmax=max(x2)*0.999
       

	#Check whether the user has sent a root location that is inside the disk. This is, by default, 2 cells from the end
	tstart = theta[0]

	#Append the root location as the first point in the theta array
	theta.append(tstart)

	#Now compute the starting radius
	rstart=r[0]

	#Check that we are in the computational domain
	if rstart<x2[0]:
		rstart=x2[0]*1.00001
	elif rstart>rmax:
		rstart=rmax

	#Append the root location as the first point in the r array
	r.append(rstart)

	for index in range(len(r)):	
		#Find out which cell we are in - 
		j,k,frac1,frac2=get_index(x1,x2,theta[-1],r[-1])

		#Compute the two velocity components by interpolation 
		tempv1=interpolate(j,k,frac1,frac2,v1,Ntheta)
		tempv2=interpolate(j,k,frac1,frac2,v2,Ntheta)

		# gather any desired hydro data along streamline by interpolation
		if gather_hydro_data:	
			for key in sim_dic.keys():
				Streamline[key].append(interpolate(j,k,frac1,frac2,sim_dic[key],Ntheta))

	Streamline['x'] = r*np.sin(theta)
	Streamline['z'] = r*np.cos(theta)
	if gather_hydro_data:
		for key in sim_dic.keys():
			Streamline[key] = np.array(Streamline[key])
	
	return Streamline 
	
####################################################
#
# interpolate simply performs a 2D linear interpolation to find the value of data, at point
#    x1_coord, x2_coord, on a grid of x1, x2.
#  data is an array of the data
#  x1 is normally theta, x2 is r - this code is written assuming spol. Might need rewriting or
# extending if we want to use more coordinate types
#
###########################################3


def interpolate(j,k,frac1,frac2,data,Ntheta):

	# note the first 4 inputs are the result of calling get_index() below
	# j,k,frac1,frac2=get_index(x1,x2,x1_coord,x2_coord)
	
	if Ntheta==1:  #Theta array is only 1 long, so we have 1D array
		d1=data[0][k]
		d2=data[0][k+1]
		d3=data[0][k]
		d4=data[0][k+1]	
	else:        # We have 2D data
		d1=data[j+1][k]
		d2=data[j+1][k+1]
		d3=data[j][k]
		d4=data[j][k+1]

	#frac1 and frac2 are the fractional distance in theta and r. We now interpolarte first in
	#theta, at the upper and lower r boundaries, then we interpolate those two points in r
	#to obtain the final result.

	upper=d1+frac2*(d2-d1)
	lower=d3+frac2*(d4-d3)
	ans=lower+frac1*(upper-lower)

	return (ans)

#################################################################
#
# Get index is a simple routine which just gives j,k where x1_coord 
#  lies between x1[j] and x1[j+1] and the same for k. It is used in interpolate, logterp
#   and any other place where we want to know which cell a coordinate is in and how far
#   it is from the boundaries in the two coordinates.
#    It was written and tested for spherical polar coordinates, but is probably general
#
#################################################################  

def get_index(x1,x2,x1_coord,x2_coord):
	if len(x1)==1: #Deals with 1D case where theta is not a variable
		j=0
		frac1=0.0
	else:      #Find the theta cell by just stepping up through the cells to find a bracket
		for j in range(len(x1)-1):
			if x1_coord>=x1[j] and x1_coord<=x1[j+1]:
				frac1=(x1_coord-x1[j])/(x1[j+1]-x1[j])
				break
	for k in range(len(x2)-1):  #Find the r cell in the same simple way
		if x2_coord>=x2[k] and x2_coord<x2[k+1]:
			frac2=(x2_coord-x2[k])/(x2[k+1]-x2[k])
			break

	# These next lines were written to deal with problems when a point was essentially on the inner
	# our outer boundary. This caused problems with this simple routine, and so the following lines
	# just set it onto the boundary by brute force if it is close.

	if x1_coord-x1[0]<x1[0]/1e6: #There is some numerical problem....
		j=0
		frac1=0.0
	if x1[-1]-x1_coord<x1[-1]/1e6:
		j=len(x1)-2
		frac1=1.0
	if x2_coord-x2[0]<x2[0]/1e6:
		k=0
		frac2=0.0
	if x2[-1]-x2_coord<x1[-1]/1e6:
		k=len(x2)-2
		frac2=1.0


	return (j,k,frac1,frac2)




