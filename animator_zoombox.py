import os
from pylab import *  # imports numpy as np and matplotlib
import athena_read   # Athena++'s reader
import array as ar   # for integer arrays

# custom routines
import hydro_calcs; reload(hydro_calcs)
import streamline_2d_fast as stream; reload(stream)
import sim_dics; reload(sim_dics)
import fig_layouts; reload(fig_layouts)
import athenaSetup_mp13 as inputs 

#====================================================#
#  REQUIRED INPUTS 
#====================================================#

ANIMATE = 1			# set to 0 for a static plot
MAKE_MOVIE = 0 		# ANIMATE must also be 1 

# select features to overplot
VEL_ARROWS = 1		# arrows depicting velocity field
VEL_STREAM = 0		# velocity streamlines 
MAG_FIELDS = 0		# magnetic field lines
MACH1_SURF = 0		# sonic surface
L_CONTOURS = 0		# specific angular momentum contours
D_FLR_SURF = 0		# density floor contour
P_FLR_SURF = 0		# pressure floor contour
B_FLR_SURF = 0		# beta floor contour
ALFVEN1_SF = 0      # super Alfvenic surface

# inputs to fig_layouts
vars = ['d','v_r','v_t','vp','T','p','cs'] 	# variables to collect (only first will be plotted)
log_vars = [1] 								# take log of vars[0]
cscheme = 'terrain_r' 						# cmap - favorites are 'Spectral', 'inferno', & 'gist_heat_r'
Ncontours = 41								# set to None to use pcolormesh instead of contourf
auto_ticks = 0								# 0 uses custom_levels_and_ticks() in fig_layouts.py 

# settings for the basic figure setup 
SETTINGS = {}
SETTINGS['VERTICAL_CBAR'] = 1	 # currently 1 is the only option
SETTINGS['RAW_GRID_DATA'] =	0    # set to 1 to plot data in i,j format
SETTINGS['REFLECTING_BC'] = 0    # set to 1 to plot from [0,pi/2], 0 for [0,pi]

# specify which dumps to animate
i_cbar = 2  # set cbar limits based on this dump
i_dumps = ar.array('i',(i for i in range(0,50,1)))

# specify paths to the runs 
run_paths = {}
run_paths['1'] = ''

# specify output dump tags
dump_tags = {}
dump_tags['1'] = 'bondi.out2.'

# specify tags for user ouput variables 
uov_tags = {}
uov_tags = None

run_names = ['data_JHcontest2019','data_JHcontest2019']
gamma = 1.666 # adiabatic index

# set distance and time units
parsec = 3.0857e18		# kiloparsec in cm
Myr = 1e6*365*24*3600 	# 10^6 years in seconds 
dt = inputs.dt*inputs.t_units/Myr

# zoom-in to this distance
R_max = 1.3
box_width = 0.3
R_max = 1.3*inputs.r_B*inputs.R_s/parsec
box_width = 0.3*inputs.r_B*inputs.R_s/parsec
R_zooms = [R_max,box_width]


#====================================================#
#  OPTIONAL INPUTS 
#====================================================#

""" plot titles """

fig_title = 'HP15 MHD' 
plot_titles = [r'$\sqrt{\sin(\theta)}$' + ' (Courant # = 0.2)', r'$\sqrt{\sin(\theta)}$'+ ' (Courant # = 0.4)']

fig_title = 'Multiphase accretion onto a supermassive black hole' #r'$\gamma=1.01$' + ' runs' 
plot_titles = run_names #['','']

plot_titles = ['',''] #['Full domain', 'Rectangular region']

""" specify streamline properties """

# locations of streamlines/fieldlines
z0 = 0.01 # height of B-fieldline footprint
eps = 0.1 # height of vel-streamline will be 2.*eps*footprints
footprints = np.array([0.51,1,1.5,2,2.5,3,3.5,4,4.5])

""" specify velocity arrow properties """

if SETTINGS['RAW_GRID_DATA']:
	vmax_black = 2.5e-3 
	Narrows_x = 15			# number of arrows in x dir
	Narrows_z = 15			# number of arrows in z dir
	rskip, tskip = 15,15 # skip this many indices when plotting velocity arrows
else:
	vmax_black = 1e-3		# characteristic velocity to scale arrow length
	vmax_black_R = 2e-3		# characteristic velocity to scale arrow length - right panel
	Narrows_x = 15			# number of arrows in x dir
	Narrows_z = 15			# number of arrows in z dir
	rskip, tskip = 5,5 		# skip this many indices when plotting velocity arrows

v_units = 1. 				# multiply velocities by this factor
bdy_pad = 1. 				# cutoff so arrows don't extend beyond domain

# only plot arrows for indices i>i_min and j>j_min
i_min =  100 
j_min = 0

""" specify density/pressure floors """
d_floors = [1.6e-7,1.6e-7] 
p_floors = [1e-10,1e-15]
beta_floors = [1e-7,1e-7] 

# Gather diagnostics at this radius index
i_mark = None
j_mark = None


#====================================================#
#  SETUP - DON'T TOUCH THIS!  
#====================================================#

""" Load Athena grid data """	

Ndumps = len(i_dumps)

# get paths to both sims
dump_dirs = sim_dics.dump_dir_dic(run_names=run_names,run_paths=run_paths,dump_tags=dump_tags,uov_tags=uov_tags)

# load grid dictionaries from both sims
GridDics = sim_dics.generate_grid_data(dump_dirs,i_dumps[0],R_zooms=R_zooms) #rskip,tskip)
grid_dic_L, grid_dic_R = GridDics['1'],GridDics['2'] 

# cell-centered r and theta axes
rc_L,tc_L = grid_dic_L['r_c'],grid_dic_L['theta_c']
rc_R,tc_R = grid_dic_R['r_c'],grid_dic_R['theta_c']
r_min, r_max = np.min(rc_L),np.max(rc_L)
theta_min,theta_max = np.min(grid_dic_L['theta_c']),np.max(grid_dic_L['theta_c'])

# determine which variables to load based on inputs
var_L = vars[0]
if len(vars) == 1:
	var_R = var_L
else:
	var_R = vars[1]
if VEL_ARROWS or VEL_STREAM:
	vars.append('v_r')
	vars.append('v_t')
if not SETTINGS['VERTICAL_CBAR']:
	vars.append('vp')
if MACH1_SURF:
	vars.append('Mp')
if L_CONTOURS:
	vars.append('l_p')
if D_FLR_SURF:
	vars.append('d')
if P_FLR_SURF:
	vars.append('p')
if B_FLR_SURF:
	vars.append('beta')
if ALFVEN1_SF:
	vars.append('vp')
	vars.append('v_A')
	
# load a full grid when calculating field lines
if MAG_FIELDS:
	GridDics_B = sim_dics.generate_grid_data(dump_dirs,i_dumps[0])


""" Figure setup plus a hack function """	

# finish adding to SETTINGS  
SETTINGS['vars'] = vars
SETTINGS['log_vars'] = log_vars
SETTINGS['auto_ticks'] = auto_ticks
SETTINGS['i_cbar'] = i_cbar
SETTINGS['i_dump0'] = i_dumps[0]
SETTINGS['cscheme'] = cscheme
SETTINGS['Ncontours'] = Ncontours
SETTINGS['fig_title'] = fig_title
SETTINGS['plot_titles'] = plot_titles
SETTINGS['vmax_black'] = vmax_black
SETTINGS['box_width'] = box_width
SETTINGS['ymax'] = R_max

SETTINGS_L = SETTINGS.copy()
SETTINGS_R = SETTINGS.copy()

# apply any different settings 
SETTINGS_R['RAW_GRID_DATA'] = 0
SETTINGS_R['vmax_black'] = vmax_black_R

# unpack all needed grids
for SETTINGS,grid_dic in zip([SETTINGS_L,SETTINGS_R],[grid_dic_L,grid_dic_R]):
	
	if SETTINGS['RAW_GRID_DATA']: # plot using (i,j) values
		xgrid, ygrid = grid_dic['i_grid'], grid_dic['j_grid']
		xcgrid, ycgrid = xgrid, ygrid
		MAG_FIELDS = 0 # mag-fields don't work with this setting
	else: # coordinate grid
		xcgrid, ycgrid = grid_dic['x_c'], grid_dic['y_c']
		if Ncontours is None:
			xgrid, ygrid = grid_dic['x'], grid_dic['y']
		else:
			if SETTINGS['REFLECTING_BC']:
				xgrid, ygrid = grid_dic['x'][0:-1,0:-1], grid_dic['y'][0:-1,0:-1]
			else:
				xgrid, ygrid = xcgrid, ycgrid

	# x/y grids are needed in SETTINGS
	SETTINGS['xgrid'], SETTINGS['xcgrid'] = xgrid,xcgrid
	SETTINGS['ygrid'],SETTINGS['ycgrid'] = ygrid,ycgrid 

# make the figure
fig,axL,axR,cbar,levels = fig_layouts.side_by_side(SETTINGS_L,SETTINGS_R,dump_dirs,GridDics)

# need this
cbar = None

# Function to clear the plot window so animations work
def clear_axis(cbar):
	Nlines = len(axL.lines)
	# remove all line1D and line2D plots
	if Nlines > 0: 
		for n in range(Nlines):
			del axL.lines[0]
	Nlines = len(axR.lines)
	if Nlines > 0: 
		for n in range(Nlines):
			del axR.lines[0]
	# remove the colorbar
	if cbar != None:
		cbar.remove()
	# remove any velocity arrows
	if len(axL.collections) > 0:
		del axL.collections[:]
	if len(axR.collections) > 0:
		del axR.collections[:]

#Display time on window
time_template = 't = %.2f [Myr]'
# time_text = ax.text(0.95, 1.04, '', transform=axR.transAxes, \
# bbox=dict(facecolor='gray', alpha=0.25, edgecolor='black', boxstyle='round,pad=1'))
time_text = axL.text(0.06, 0.96, '', transform=axL.transAxes)

#====================================================#
#  ANIMATION ROUTINE - ADD NEW FEATURES AS DESIRED
#====================================================#

""" Plotting routine """	
		    		
def plotter2D(index):
	global cbar,vars,qty,MAG_FIELDS,SimDic
	clear_axis(cbar)
	
	# load simulation data
	SimDic = sim_dics.load_vars(i_dumps[index],vars,dump_dirs,GridDics,gamma=gamma)

	for SETTINGS,grid_dic,ax,key in zip([SETTINGS_L,SETTINGS_R],[grid_dic_L,grid_dic_R],[axL,axR],['1','2']):
		# update time
		time = dt*float(i_dumps[index]) # time in code units
		time_text.set_text(time_template%(time))

		# load quantities for the colormap plots
		xgrid,ygrid = SETTINGS['xgrid'],SETTINGS['ygrid']
		xcgrid,ycgrid = SETTINGS['xcgrid'],SETTINGS['ycgrid']
		Sim = SimDic[key]
		qty = Sim[vars[0]]
		if key=='1':
			sign = -1
		else:
			sign = 1
		
		# print min/max of qty to screen
		print("key = {}, index = {}, time = {}, min(qty) = {}, max(qty) = {}").format(key,index,time,np.min(qty),np.max(qty))
		
		# user setting: take log of quantities
		if log_vars[0]:
			if np.min(qty) < 0.:
				qty = np.log10(np.abs(qty))
			else:
				qty = np.log10(qty)
			
		# colormap plot - plots map of qty (the first variable listed in vars)
		if Ncontours is None:
			ax.pcolormesh(xgrid, ygrid, qty, cmap=cscheme)
		else:
			ax.contourf(xgrid,ygrid,qty,levels,cmap=cscheme,extend='both')

		if MACH1_SURF: # Contour plot of Mach 1 surface
			j_mid = len(xcgrid[:,0])
			ax.contour(xcgrid[0:j_mid,:], ycgrid[0:j_mid,:], SimDic[key]['Mp'][0:j_mid,:],levels=[1.],linewidths=1,colors='cyan',linestyle='--')
			
		if ALFVEN1_SF: # Contour plot of Mach 1 surface
			j_mid = len(xcgrid[:,0])
			contour = SimDic[key]['vp'][0:j_mid,:]/SimDic[key]['v_A'][0:j_mid,:]
			ax.contour(xcgrid[0:j_mid,:], ycgrid[0:j_mid,:], contour,levels=[1.],linewidths=2,colors='b')

		if L_CONTOURS: # Contour plot of specific angular momentum
			ls = [1,2,4,8,16,32]
			shades_of_gray = np.linspace(0.1,1.,len(ls)+1,dtype='|S3')
			shades_of_gray = shades_of_gray[::-1]
			ax.contour(xcgrid, ycgrid, np.abs(SimDic[key]['l_p']),levels=ls,linewidths=1.5,colors=shades_of_gray)
		
		if D_FLR_SURF: # Contour plot density floor surface
			r_test = 30.
			d_floor0 = d_floors[0]/r_test**2
			d_floor1 = d_floors[1]/r_test**2
			
			# plot a circle at r_test marking the local density floor
			ax.contour(xcgrid, ycgrid, grid_dic['r_c_grid'],levels=[r_test],linewidths=2,ls='-',colors='white')
			
			# if these contours intersect the above circle, the density floor is hit
			ax.contour(xcgrid, ycgrid, SimDic[key]['d'],levels=[d_floor0],linewidths=2,ls='--',colors='black')
			
		if P_FLR_SURF: # Contour plot density floor surface
			ax.contour(xcgrid, ycgrid, SimDic[key]['p'],levels=[p_floors[0]],linewidths=2,colors='red')
			
		if B_FLR_SURF: # Contour plot density floor surface
			ax.contour(xcgrid, ycgrid, SimDic[key]['beta'],levels=[beta_floors[0]],linewidths=2,colors='white')
			
		if VEL_ARROWS:
			global r_c,theta_c,vx_grid,vz_grid,vx_vals,vz_vals

			v_r, v_t = Sim['v_r'], Sim['v_t']
			vx = (v_r*grid_dic['sin_thetas'] + v_t*grid_dic['cos_thetas'])*v_units
			vz = (v_r*grid_dic['cos_thetas'] - v_t*grid_dic['sin_thetas'])*v_units
			
			if SETTINGS['RAW_GRID_DATA']:
				xgrid, ygrid = grid_dic['i_grid'][j_min::tskip,i_min::rskip], grid_dic['j_grid'][j_min::tskip,i_min::rskip]
				ax.quiver(xgrid, ygrid, sign*v_r[j_min::tskip,i_min::rskip], v_t[j_min::tskip,i_min::rskip], color='k', units='width',scale=vmax_black, scale_units='inches')
			else:
				# Plot velocity direction arrows over the top of the already generated density plot
				i_zoom = grid_dic['i_zoom']
				rmax1=grid_dic['r_c'][i_zoom]
				r_c,theta_c = grid_dic['r_c'],grid_dic['theta_c']
	
				#We will plot arrows over a slightly smaller grid to keep things looking nice.
				vx_grid=np.linspace(bdy_pad*r_min*np.sin(theta_max),rmax1,Narrows_x)
				vz_grid=np.linspace(-rmax1,rmax1,2*Narrows_z)
				vx_vals=np.zeros([len(vx_grid),len(vz_grid)])
				vz_vals=np.zeros([len(vx_grid),len(vz_grid)])
				for i in range(len(vx_grid)):
					for j in range(len(vz_grid)):
						r_test=np.sqrt(vx_grid[i]**2+vz_grid[j]**2)
						theta_test=np.arctan(vx_grid[i]/vz_grid[j])
						if theta_test < 0.:
							theta_test += np.pi
						jj,ii,d_j,d_i = hydro_calcs.get_index(theta_c,r_c,theta_test,r_test)
						if r_test < r_max and r_test > r_min and theta_test > theta_min and theta_test < theta_max:
							vx_temp=hydro_calcs.interpolate_fast(jj,ii,d_j,d_i,vx)
							vz_temp=hydro_calcs.interpolate_fast(jj,ii,d_j,d_i,vz)
							vx_vals[i][j]=vx_temp 
							vz_vals[i][j]=vz_temp 
						else:
							vx_vals[i][j]=0.0
							vz_vals[i][j]=0.0
				ax.quiver(vx_grid, vz_grid, sign*np.transpose(vx_vals), np.transpose(vz_vals), color='k', units='width',scale=vmax_black, scale_units='inches')
					
		if MAG_FIELDS:
			global Bdics
			print "[MAG_FIELDS]: i_dumps = {}, ii = {}".format(i_dumps[index],index)
			Bdics = sim_dics.load_vars(i_dumps[index],['B_r','B_t'],dump_dirs,GridDics_B,MHD_check=1)

	  		if 'B_r' in Bdics[key].keys(): # this ensures this is an MHD run
		  		Grid = GridDics_B[key]
		  		B_r = Bdics[key]['B_r']
				B_t = Bdics[key]['B_t']
				
				r_c,theta_c = Grid['r_c'],Grid['theta_c']
		  		
				for f in footprints:
					start = [f,z0]
					if f < 0.05:
						start = [f,np.cos(np.arcsin(f)) + z0]
					Fieldline = stream.fieldline_2d(i_dumps[index],dump_dirs[key+'path'],Grid,B_r,B_t,theta_c,r_c,start,'spol',-1,-2,RECALC=0)
					ax.plot(Fieldline['x'],Fieldline['z'],color='m',ls='-',lw=1,alpha=1.)
			else:
				print "[MAG_FIELDS]: Skipping {}, which is not an MHD run!".format(dump_dirs[key+'path'] )	
		
		if VEL_STREAM:
			global Streamline
			StreamDics = sim_dics.load_vars(i_dumps[index],['d','p','v_r','v_t','v_p','Be'],dump_dirs,GridDics)

			v_r = StreamDics[key]['v_r']
			v_t = StreamDics[key]['v_t']
			r_c,theta_c = grid_dic['r_c'],grid_dic['theta_c']
	  		
			for f in footprints:
				h = 2.2*eps*f
				start = [f,h]
				if f < 1.:
					start = [f,np.cos(np.arcsin(f)) + z0]
				
				Streamline = stream.streamline_2d(i_dumps[index],dump_dirs[key+'path'],grid_dic,v_r,v_t,theta_c,r_c,start,'spol',-1,-2,RECALC=0,sim_dic=StreamDics[key])
				ax.plot(Streamline['x'],Streamline['z'],color='gray',ls='-',lw=1)
		
		# plot a colored line near disk surface and several symbols at i_mark	
		if i_mark is not None: 

			ax.plot(rc[0:i_max]*grid_dic['sin_thetas'][j_mark,:],rc[0:i_max]*grid_dic['cos_thetas'][j_mark,:],'b--')
			ax.plot(rc[i_mark]*grid_dic['sin_thetas'][j_mark-5,i_mark],rc[i_mark]*grid_dic['cos_thetas'][j_mark-5,i_mark],ls='',marker='+',markersize=15,color='white')
			ax.plot(rc[i_mark]*grid_dic['sin_thetas'][j_mark-10,i_mark],rc[i_mark]*grid_dic['cos_thetas'][j_mark-10,i_mark],ls='',marker='+',markersize=15,color='white')
			ax.plot(rc[i_mark]*grid_dic['sin_thetas'][j_mark-15,i_mark],rc[i_mark]*grid_dic['cos_thetas'][j_mark-15,i_mark],ls='',marker='+',markersize=15,color='white')

		if 0:  # plot a diagonal line at theta_deg
			global x,y
			r_c = grid_dic_R['r_c']
			theta_deg = 82.
			theta = (90. - theta_deg)*np.pi/180.
			x = r_c*np.cos(theta)
			y = r_c*np.sin(theta)
			ax.plot(x,y,'k--')


""" Code to save image, animate, or make movie """

if ANIMATE:
	import matplotlib.animation as animation
	ani = animation.FuncAnimation(fig,plotter2D,init_func=None,
    frames=Ndumps,interval=100, blit=False,repeat=False)
	
	if len(i_dumps) == 1:
		savefig('afig_[animator_2panel].png')
    	
else:
	plotter2D(0)
	savefig('afig_[animator_2panel].png')

if MAKE_MOVIE: # requires ffmpeg
	default_name = `i_dumps[0]` + '-' + `i_dumps[-1]` \
	 + '_' + run_names[0] + '_' + vars[0]
	if not os.path.exists('movies'):
		os.makedirs('movies')
	name = 'movies/' + default_name + '.mp4'
	# for higher-res, use bitrate=5000
	ani.save(name, fps=10, bitrate=3000)
else: 
	show()
