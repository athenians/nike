import numpy as np

def optical_depth_grid(grid_dic,rho,tau_units,j_surf,j_surf_last,Nlayers=1,tau_shield=0,ATTENUATION=1,REFLECTING_BCS=1):
	r_c = grid_dic['r_c']
	tau_grid = np.zeros_like(rho)
	
	drs = grid_dic['r'][1:]-grid_dic['r'][:-1]
	if ATTENUATION:
		for j in range(len(rho[:,0])):
			for i in range(1,len(r_c)+1):
				tau_grid[j,i-1] = np.sum(rho[j,0:i]*drs[0:i])
	
	tau_grid = tau_units*tau_grid
	
	# add extra optical depth to the disk region
	if REFLECTING_BCS:
		tau_grid[j_surf+Nlayers+1:] += tau_shield
	else:
		tau_grid[j_surf+Nlayers+1:j_surf_last+1-(Nlayers-1)] += tau_shield
		
	return tau_grid
		
def mtot_spherical(r,theta,rho,i_max=-1,j_max=-1):
	""" 
	Computes the total mass (\int rho dV) in spherical coords
	assuming an axisymmetric domain from [0,pi/2]
	at the radius r0 over theta indices from 0 to j_max
	- r,theta are 1D arrays of length Nr,Nt
	- rho is a 2D array with shape (Nr,Nt)
	- by default, i_max=-1 means integrate over all radii
	- by default, j_max=-1 means integrate over all theta
	"""
	
	# optional: calc tot mass only to j_max
	if j_max > 0:
		mu = np.cos(theta[0:j_max+1])
	elif j_max < -1:
		mu = np.cos(theta[abs(j_max):])
	else:
		mu = np.cos(theta)
	mu = mu[::-1] # reverse the array to make mu go from 0 to 1
	dmus = mu[1:] - mu[0:-1]
	
	# optional: calc tot mass only to i_max
	if i_max > 0:
		drs = r[1:i_max+1] - r[0:i_max]
	else:	
		drs = r[1:] - r[0:-1]
	r_c = r[0:i_max] + 0.5*drs # cell-centered radii
	
	mtot = 0.
	for i in range(len(drs)):
		dVs = 2.*np.pi*r_c[i]**2*drs[i]*dmus
		if j_max > 0:
			rhos = rho[0:j_max,i]
		elif j_max < -1:
			rhos = rho[abs(j_max):,i]
		else:
			rhos = rho[:,i]
		
		mtot += sum(rhos*dVs)

	return mtot
	
def mdot_rsurf(r,theta,rho_vr,r0=-1,j_max=-1):
	""" 
	Computes the total mass loss rate in spherical coords
	assuming an axisymmetric domain from [0,pi/2]
	at the radius r0 over theta indices from 0 to j_max
	- r,theta are 1D arrays of length Nr,Nt
	- rho_vr = rho*v_r is a 2D array with shape (Nr,Nt)
	- by default, r0 = -1 computes mdot at r_out 
	- by default, j_max=-1 means integrate over all theta
	"""
	
	# radius index to use
	if r0 == -1: # last active zone
		idx = -1
	elif r0 == 0: # first active zone
		idx = 0
	else: # determine r0's index 
		idx = (np.abs(r - r0)).argmin()
		
	# calc mu and mass flux density arrays from 0,j_max
	if j_max > 0:
		mu = np.cos(theta[0:j_max+1])
		mfd = rho_vr[0:j_max,idx]
	else:
		mu = np.cos(theta)
		mfd = rho_vr[:,idx]
	
	mu = mu[::-1] # reverse the array to make mu go from 0 to 1
	dmu = mu[1:] - mu[0:-1]
# 	dmdot = 0.5*(mfd[0:-1] + mfd[1:])*dmu*r[idx]**2
	dmdot = mfd*(2.*np.pi*r[idx]**2)*dmu
	
	# return both inflowing and outflowing
	if r0 == -1:
		outflowing = dmdot > 0.
		mdot_out = sum(dmdot[outflowing])
		
		inflowing = dmdot < 0.
		mdot_in = sum(dmdot[inflowing])
	elif r0 == 0:
		outflowing = dmdot < 0.
		mdot_out = sum(dmdot[outflowing])
		
		inflowing = dmdot > 0.
		mdot_in = sum(dmdot[inflowing])
		
# 	return sum(dmdot)
	return np.abs(mdot_out),np.abs(mdot_in)
		
def mdot_midplane(r,rho_vt,j=-1):
	""" 
	Computes the total mass loss rate in spherical coords
	assuming an axisymmetric domain from [0,pi/2]
	at the radius r0 over theta indices from 0 to j_max
	- r,theta are 1D arrays of length Nr,Nt
	- rho_vr = rho*v_r is a 2D array with shape (Nr,Nt)
	- by default, r0 = -1 computes mdot at r_out 
	- by default, j_max=-1 means integrate over all theta
	"""
	mfd = rho_vt[j,:]
	dr = r[1:] - r[0:-1]
	r_c = r[:-1] + 0.5*dr # cell-centered radii
	dmdot = mfd*(2.*np.pi*r_c)*dr
	
	# return both inflowing and outflowing
	if j == 0:
		outflowing = dmdot < 0.
		mdot_out = sum(dmdot[outflowing])
		
		inflowing = dmdot > 0.
		mdot_in = sum(dmdot[inflowing])
	else:
		outflowing = dmdot > 0.
		mdot_out = sum(dmdot[outflowing])
		
		inflowing = dmdot < 0.
		mdot_in = sum(dmdot[inflowing])
	
		
	return np.abs(mdot_out),np.abs(mdot_in)

def evaluate_Q(i,k,phi):
	"""
	Inputs: radial index, polar index, resonant phi angle
	Outputs:
	Q is the velocity gradient along the line of sight.
	"""
	r_inv = 1./rcs[i]
		
	# define algebraic combinations of angles
	cos_phi = np.cos(phi)
	alpha = sin_thetas[k]*sini*cos_phi + cos_thetas[k]*cosi
	beta = cos_thetas[k]*sini*cos_phi - sin_thetas[k]*cosi
	sinicosphi = sini*np.cos(phi)
	sinisinphi = sini*np.sin(phi)
	cot_theta = cos_thetas[k]/sin_thetas[k]
	
	if inputs.ZERO_VZ:
		beta = 0.

	# components of strain tensor in spherical coordinates
	# note: each component is multiplied by sin(theta) as 
	e_rr = vr_prime_r[i,k]
	e_tt = r_inv*(vtheta_prime_theta[i,k] + vr[i,k])
	e_pp = r_inv*(vr[i,k] + vtheta[i,k]*cot_theta)
	e_rt = 0.5*(vtheta_prime_r[i,k] + r_inv*(vr_prime_theta[i,k] - vtheta[i,k]))
	e_rp = 0.5*(vphi_prime_r[i,k] - vphi[i,k]*r_inv)
	e_tp = 0.5*r_inv*(vphi_prime_theta[i,k] - vphi[i,k]*cot_theta)
	
	Q = alpha**2*e_rr + beta**2*e_tt + sinisinphi**2*e_pp + \
		2.*(alpha*beta*e_rt - alpha*sinisinphi*e_rp - beta*sinisinphi*e_tp)
	
	return np.abs(Q)


def multi_stream(athena_dic,wroot,zroot=(),picklefile="",tstart=-2,area_flag="n",rmax=-1,npoints=100000):

# Extract the arrays we will be using from the dictionary
	r,theta = athena_dic[0],athena_dic[1]
	r = r[:-1] + 0.5 * np.ediff1d(r)
	theta = theta[:-1] + 0.5 * np.ediff1d(theta)
	v_r = athena_dic[3]['vel'][0,:,:,0]
	v_theta = athena_dic[3]['vel'][0,:,:,1]

# If the user didnt define rmax, just set it to the outer edge
	if rmax==-1:
		rmax=max(r)

#We will be numbering the streamlines, the first is number zero
	nstream=0

#Set up empty arrays to hold all the streamlines

#First the coordinates
	streamw=[]
	streamz=[]
#Now the area of the streamline
	area_stream=[]
#We save the original roots
	roots=[]
#A flag to say wether we calculated areas
	area_flag1=[]
#The area can't necessarily be calculated for all z points, this tells us which z point first has an area
	area_min_z=[]
#The end condition of each streamline (hit disk, escaped, stalled etc)
	status=[]

#Do a loop over all requested roorts
	for i in range(len(wroot)):
#If we have zroots for all wroots, then use them
		if len(zroot) == len(wroot):
			start =(wroot[i],zroot[i])
#Otherwise calculate the zroot from the disk surface
		else:	
			start =(wroot[i],wroot[i]*(np.tan(np.pi/2.0-theta[tstart])))
#We try to generate a stream, this try command will crash if any of the commands before except fail. Obviously,
#this is most likely to be the call to streamline2d.
		try:
#Just to tell the user where we are - it can take a while to do this...
			print "Multi stream: computing stream ",i," of ",len(wroot)
#This is the call to compute the streamline
			w,z,stat=streamline_2d(v_r,v_theta,theta,r,start,rmax=rmax,tdisk=tstart,npoints=npoints)
#Check if we want an area for the streamline
			if area_flag=="y" and len(w)>1:
#If so, remind the user what we are doing, and call stremline_area
				print "Multi stream: computing stream area ",i," of ",len(wroot)
				area,min_z=streamline_area(v_r,v_theta,theta,r,start,data["Coord_sys"],w=w,z=z,tdisk=tstart,npoints=npoints)
#If not, fill the area array with dummy data
			else:
				area=[-1]
#If something went wrong, we hold the error code and report it, hopefully the user can work out what went wrong
		except Exception as e:
			print "Error computing stream:",str(e)
			if len(w)==0:
				print "Error multi stream - cannot compute streamlines for w0=",wroot[i]
				w=[]
				z=[]
				stat="error"
#This would be odd, it means that there was an error along the streamline, well, lets tell the user but keep the data
			else:
				print "For some reason the streamline from w0=",wroot[i]," was truncated. Status=",stat 
#If we got a nice streamline, tell the user how much work it was to do it, then copy the data to all of the 
#Arrays. NB, these end up as lists of arays/	
		if len(w)>1:
			print "Stream ",i," has length ",len(w)
			roots.append(start)
			streamw.append(np.array(w))
			streamz.append(np.array(z))
			status.append(stat)
#If we asked for area, and got area, append all that data, set the flags accordingly.
			if area_flag=="y" and len(area)>0 and area[0]!=-1:
				area_stream.append(np.array(area))
				area_flag1.append(1)
				area_min_z.append(min_z)
			else:
				area_stream.append(np.array([-1]))
				area_flag1.append(0)
				area_min_z.append(-1)
#Move onto the next streamline
			nstream=nstream+1

#Turn the data into arrays - easier to address in future.
	streamw=np.array(streamw)
	streamz=np.array(streamz)
	area_stream=np.array(area_stream)
	area_flag1=np.array(area_flag1)
	area_min_z=np.array(area_min_z)



#We will save this data - it can take a while to generate for lots of streamlines. first, we neatly package it in
#a dictionary.
	

	stream_data={'sw':streamw,'sz':streamz,'area':area_stream,'area_flag':area_flag1,'area_min_z':area_min_z}
	stream_data["roots"]=np.array(roots)
	stream_data["type"]='stream'
	stream_data["nstream"]=nstream
	stream_data["status"]=status
#If we have been given a filename, create the pickle file and save it. We dont test wether the file exists here, 
#so be careful to check in the calling routine. This one is dumb, it just overwrites.
	if picklefile != "":
		savefile=open(picklefile,'wb')
		pickle.dump(stream_data,savefile)
		savefile.close()
		return()
#If there wasnt a savefile - return the dictionary
	else:
		return (stream_data)
		
		
		
def streamline_2d(v1,v2,x1,x2,start,rmax=-1,tdisk=-2,npoints=100000):

# If rmax is not supplied, or is greater than the maximum radius in the simulation, set it to just inside the outer radius.
	if rmax==-1 or rmax>=max(x2):
		rmax=max(x2)*0.999

# Set up two arrays to hold the r, theta coordinates of the streamline
	r=[]
	theta=[]	

#First, compute the theta coordinate of the start of the streamline
#start[1] is the y coordinate. We need to deal with the situation where a user asks for y=0, this will give an infinity 
	if start[1]==0.0:
		tstart=np.pi/2.0   #Deal with potential infinity
	else:
		tstart=np.arctan(start[0]/start[1])

#Check whether the user has sent a root location that is inside the disk. This is, by default, 2 cells from the end
	if tstart>x1[tdisk]:
		tstart=x1[tdisk]*0.9999999
		print "Warning: Streamline_2d - requested start point in disk - resetting to just above disk"
	
#Append the root location as the first point in the theta array
	theta.append(tstart)

#Now compute the starting radius
	rstart=np.sqrt(start[1]**2+start[0]**2)\

#Check that we are in the computational domain
	if rstart<x2[0]:
		rstart=x2[0]*1.00001
	elif rstart>rmax:
		rstart=rmax

#Append the root location as the first point in the r array
	r.append(rstart)



# Set ap a grid of length scales for the calculation - this is used to ensure that we choose sensible time steps for each cell.
#We will divide the length scale in a cell by the maximum velocity in the cell to give a time to cross the cell. We then divide by 10.
#We calculate the length of each side of the wedge shaped cell, then set the length scale to the smallest length

#Set up an empty array
	lengths=np.ones([len(x1),len(x2)])
#Loop over the r and theta arrays
	for j in range (len(x1)):
		for k in range(len(x2)):
			if j>0:	
				dl1=np.abs(x2[k]*np.cos(x1[j])-x2[k]*np.cos(x1[j-1]))
			else:
				dl1=np.abs(x2[k]*np.cos(x1[j])-x2[k]*np.cos(x1[j+1]))
			if k>0:
				dl2=np.abs(x2[k]-x2[k-1])
			else:
				dl2=np.abs(x2[k]-x2[k+1])

			lengths[j][k]=np.min([dl1,dl2])


	push=1.01   #This is a variable to try to ensure we dont end up exactly on a boundary
	status="tracking" #This flag is set to tracking while we are following a streamline. Any condition that means we want to stop following should result in this flag being changed to something else, and the loop being exited at the end.
	i=0
	while status=="tracking":	

#Compute the two velocity components by interpolation 
		tempv1=interpolate(x1,x2,theta[-1],r[-1],v1)
		tempv2=interpolate(x1,x2,theta[-1],r[-1],v2)

#Find out which cell we are in - this is just used to get the relvant length scale
		j,k,frac1,frac2=get_index(x1,x2,theta[-1],r[-1])

#We step across a cell my moving lengths given by the velocity x a time. This next line computes a time step for
#the current cell, simply by taking the smallest length, and dividing by the fastest velocity, and then by 10.	
		dt=(lengths[j][k]/np.max(np.abs([tempv1,tempv2])))/10.0

#Compute the new theta and r coordinates, just multiply the relevant velocities by the time step.
#We will check that these two 
		tnew=theta[-1]+(np.arctan(tempv2/r[-1])*dt)
		rnew=r[-1]+tempv1*dt


#We are now going to do a little testing. We want to make sure that if we enter a new cell, we dont jump too 
#far across that new cell.
#We begin by setting two new times, that we will use in a minute. Set the to the age of the universe as default
#NB, this was the age of the universe (to 1SF) as of 23rd March 2015. 
		r_time=4e17
		theta_time=4e17

#The following tests see if we will go up or down a cell in r or theta, and sets a time that will just about take us
#into the new cell. This means we won't race across a cell
		if tnew < x1[j] and theta[-1] != x1[j] :
			theta_time=push*(abs((x1[j]-theta[-1])/(np.arctan(tempv2/r[-1]))))
		if tnew > x1[j+1] and theta[-1] != x1[j+1]:
			theta_time=push*(abs((x1[j+1]-theta[-1])/(np.arctan(tempv2/r[-1]))))
		if rnew > x2[k+1]:
			r_time=push*(abs((x2[k+1]-r[-1])/tempv1))
		if rnew < x2[k]:
			r_time=push*(abs((x2[k]-r[-1])/tempv1))

#Compute new, new values of r and theta
		tnew=theta[-1]+(np.arctan(tempv2/r[-1])*min(dt,r_time,theta_time))
		rnew=r[-1]+tempv1*min(dt,r_time,theta_time)

#Now we tests to see if we have reached any of the limits that would cause us to stop tracking the streamline
#The message that the code outputs is pretty self explanatory 
		if tnew > x1[tdisk] :
			print "At upper theta limit (hit disk) after ",i," steps"
			status="hit_disk"
		elif tnew < x1[0] :
			print "At lower theta limit after ",i," steps"	
			status="hit_pole"
		elif rnew > rmax: 
			print "At outer radial boundary after ",i," steps"
			status="reached_outer"
		elif rnew < x2[0]:
			print "At inner radial boundary after ",i," steps"
			status="reached_inner"
		elif tnew==theta[-1] and rnew==r[-1]:
			print "Stalled after ",i," steps"
			status="stalled"
		elif i>npoints:
			print "Tracked for ",i," steps, aborting"
			status="too_many_steps"
		else:	
#If there are no stop conditions, append the new r and theta coordinates onto the arrays
			r.append(rnew)
			theta.append(tnew)
			i=i+1

#We want to return cartesian coordinates so loop over the r,theta coordinates and turn them back.
	x=[]
	z=[]
	for i in range(len(r)):
		x.append(r[i]*np.sin(theta[i]))
		z.append(r[i]*np.cos(theta[i]))
	return(np.array(x),np.array(z),status)
	

def interpolate(x1,x2,x1_coord,x2_coord,data):

#First we call get_index to find the number of the grid that our coords are in. This is
#broken out into a seperate routine, because sometimes we just want to know the index, without
#interpolating.

	if len(x1)==1:  #Theta array is only 1 long, so we have 1D array
		j,k,frac1,frac2=get_index(x1,x2,x1_coord,x2_coord)
		d1=data[0][k]
		d2=data[0][k+1]
		d3=data[0][k]
		d4=data[0][k+1]	
	else:        # We have 2D data
		j,k,frac1,frac2=get_index(x1,x2,x1_coord,x2_coord)
		d1=data[j+1][k]
		d2=data[j+1][k+1]
		d3=data[j][k]
		d4=data[j][k+1]

#frac1 and frac2 are the fractional distance in theta and r. We now interpolarte first in
#theta, at the upper and lower r boundaries, then we interpolate those two points in r
#to obtain the final result.

	upper=d1+frac2*(d2-d1)
	lower=d3+frac2*(d4-d3)
	ans=lower+frac1*(upper-lower)

	return (ans)

def interpolate_fast(j,i,d_j,d_i,data):

	if data.shape[0]==1:  #Theta array is only 1 long, so we have 1D array
		d1=data[0][i]
		d2=data[0][i+1]
		d3=data[0][i]
		d4=data[0][i+1]	
	else:        # We have 2D data
		d1=data[j+1][i]
		d2=data[j+1][i+1]
		d3=data[j][i]
		d4=data[j][i+1]

	upper=d1+d_i*(d2-d1)
	lower=d3+d_i*(d4-d3)
	ans=lower+d_j*(upper-lower)

	return (ans)
	
def get_index(x1,x2,x1_coord,x2_coord):




	if len(x1)==1: #Deals with 1D case where theta is not a variable
		j=0
		frac1=0.0
	else:      #Find the theta cell by just stepping up through the cells to find a bracket
		for j in range(len(x1)-1):
			if x1_coord>=x1[j] and x1_coord<=x1[j+1]:
				frac1=(x1_coord-x1[j])/(x1[j+1]-x1[j])
				break
	for k in range(len(x2)-1):  #Find the r cell in the same simple way
		if x2_coord>=x2[k] and x2_coord<x2[k+1]:
			frac2=(x2_coord-x2[k])/(x2[k+1]-x2[k])
			break
			
	# These next lines were written to deal with problems when a point was essentially on the inner
# our outer boundary. This caused problems with this simple routine, and so the following lines
# just set it onto the boundary by brute force if it is close.

	if x1_coord-x1[0]<x1[0]/1e6: #There is some numerical problem....
		j=0
		frac1=0.0
	if x1[-1]-x1_coord<x1[-1]/1e6:
		j=len(x1)-2
		frac1=1.0
	if x2_coord-x2[0]<x2[0]/1e6:
		k=0
		frac2=0.0
	if x2[-1]-x2_coord<x1[-1]/1e6:
		k=len(x2)-2
		frac2=1.0

	return (j,k,frac1,frac2)
