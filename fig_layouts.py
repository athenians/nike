from pylab import *
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
import matplotlib.lines as lines
import sim_dics

# assign global font properties
matplotlib.rcParams['font.size'] = '14'
matplotlib.rcParams['ps.fonttype'] = 42 #note: fontype 42 compatible with MNRAS style file when saving figs
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'serif' #'STIXGeneral-Regular'    
plt.rcParams['axes.labelsize'] = 16
plt.rcParams['axes.linewidth'] = 1.

# function for user to specify custom ticks/levels
def custom_levels_and_ticks():
    levels = np.linspace(1,5,25)
    ticks = np.round(levels,3) 
    ticks = ticks[::6]
    return levels,ticks

# function to calculate colorbar range/ticks
def tick_function(X,Nticks=None):
    tick_marks = ["%.2f" % v for v in X]
    tick_marks = np.array([float(t) for t in tick_marks])
    if Nticks == None:
        Nskip = max(1,int(len(tick_marks)/10.))
    else:
        Nskip = max(1,int(len(tick_marks)/Nticks))
    return tick_marks[::Nskip]
    

# function to plot two simulation quantities side-by-side
def side_by_side(SETTINGS_L, SETTINGS_R, dump_dirs, GridDics):
    """
    Makes a symmetric left-right 2-panel plot with 1 or 2 colorbars
    depending on the value of VERTICAL_CBAR = plot_options_dic['VERTICAL_CBAR']
    - for VERTICAL_CBAR = 1, plots the same quantity from 2 different sims 
    - for VERTICAL_CBAR = 0, plots two horizontal colorbars with different
                         data from the same sim 
    """
    REFLECTING_BC = 0

    i_dump0 = SETTINGS_L['i_dump0']
    i_cbar = SETTINGS_L['i_cbar']
    vars = SETTINGS_L['vars']

    # open dump i_cbar: this dump determines the colorbar limits
    SimDic = sim_dics.load_vars(i_cbar,vars,dump_dirs,GridDics)

    # open dump i_dumps[0]: this will be the first displayed pcolormesh/contourf plot
    SimDic_0 = sim_dics.load_vars(i_dump0,vars,dump_dirs,GridDics)

    if REFLECTING_BC: # for 0 to pi/2 domains 
        fig = figure(figsize=(12, 6))
        plt.subplots_adjust(left=0.06, bottom=0.1, right=0.96, top=0.88, wspace=None, hspace=None)
    else:
        fig = figure(figsize=(10, 8))
        plt.subplots_adjust(left=0.095, bottom=0.08, right=0.99, top=0.92, wspace=None, hspace=None)
    axL = fig.add_subplot(121)
    axR = fig.add_subplot(122)


    # calculate positioning to make room for colorbar
    bboxL = axL.get_position()
    ptsL = bboxL.get_points()
    widthL = ptsL[1][0] - ptsL[0][0]
    heightL = ptsL[1][1] - ptsL[0][1]
    

    bboxR = axR.get_position()
    ptsR = bboxR.get_points()
    widthR = ptsR[1][0] - ptsR[0][0]
    heightR = ptsR[1][1] - ptsR[0][1]
    gap = ptsR[0][0] - ptsL[1][0]
    
    # for each panel ...
    for plot_options_dic,ax,key in zip([SETTINGS_L,SETTINGS_R],[axL,axR],['1','2']):
        # unpack main figure setup plot options
        VERTICAL_CBAR = plot_options_dic['VERTICAL_CBAR']
        REFLECTING_BC = plot_options_dic['REFLECTING_BC']
        RAW_GRID_DATA = plot_options_dic['RAW_GRID_DATA']

        # unpack remaining plot options
        Ncontours = plot_options_dic['Ncontours']
        cscheme = plot_options_dic['cscheme']
        fig_title = plot_options_dic['fig_title']
        plot_titles = plot_options_dic['plot_titles']
        LOGVAL = plot_options_dic['log_vars'][0]
        AUTO_TICKS = plot_options_dic['auto_ticks']
        
        # decide what quantity to plot on the right panels 
        qty = SimDic[key][vars[0]]
        qty0 = SimDic_0[key][vars[0]]        

        # take any log of quantities 
        if LOGVAL:
            if np.min(qty) < 0.:
                qty = np.log10(np.abs(qty))
            else:
                qty = np.log10(qty)

            if np.min(qty0) < 0.:
                qty0 = np.log10(np.abs(qty0))
            else:
                qty0 = np.log10(qty0)


        # load all plot labels 
        label_dic = sim_dics.load_labels(LOGVAL)


        if VERTICAL_CBAR:

            # add titles and labels
            suptitle(fig_title)
            if key=='1':
                ax.set_title(plot_titles[0])
            else:
                ax.set_title(plot_titles[1])
            if RAW_GRID_DATA:
                ax.set_xlabel(r'$i$')
                ax.set_ylabel(r'$j$')
            else:
                ax.set_xlabel(r'$x$' + ' [pc]')
                if key=='1':
                    ax.set_ylabel(r'$z$' + ' [pc]')


            # hide y-ticks on right axis
            if key=='2':
                plt.setp(ax.get_yticklabels(), visible=False)
            
            # set zoom-in region 
            i_zoom = GridDics[key]['i_zoom']
            r = GridDics[key]['r']
            rc = GridDics[key]['r_c']
            tc = GridDics[key]['theta_c']

            if i_zoom == (GridDics[key]['i_max']-1):
                i_lim = GridDics[key]['i_max']
            else:
                i_lim = i_zoom 
            
            if REFLECTING_BC: # for 0 to pi/2 domains 
                if key=='1':
                    ax.axis([r[i_lim], 0., 0., r[i_lim]])
                else:
                    ax.axis([0.,r[i_lim], 0., r[i_lim]])
            else:
                if key=='1':
                    ax.axis([r[i_lim], 0., -r[i_lim], r[i_lim]])
                else:
                    ax.axis([0.,r[i_lim], -r[i_lim], r[i_lim]])
            
            if RAW_GRID_DATA:
                if key=='1':
                    ax.axis([i_zoom-1, 0, len(tc)-1, 0])
                else:
                    ax.axis([0,i_zoom-1, len(tc)-1, 0])
            else:
                #enforce same y-ticks as x-ticks
                xticks = ax.get_xticks()
                if REFLECTING_BC: # for 0 to pi/2 domains 
                    yticks = xticks
                else:
                    yticks = np.append(-xticks[::-1][1:-1],xticks[:-1])
                if key=='2':
                    ax.set_xticks(xticks[1:])


        # determine the contour levels and tick marks
        if AUTO_TICKS:
            print "imin,imax = {},{}".format(np.min(qty),np.max(qty))
            if LOGVAL:
                imin,imax = int(np.min(qty))-1,int(np.max(qty))+1

                if Ncontours is None:
                    Nlevels = (imax-imin)*100+1
                    Nl = Nlevels
                else:
                    Nticks = min(15,(imax-imin)+1)
                    Nl = Ncontours
            else:
                imin = np.min(qty_L)
                imax = np.max(qty_L)
                Nl = 100

            levels = np.linspace(imin,imax,Nl)
            ticks = tick_function(levels,Ncontours)
        else:
            levels,ticks = custom_levels_and_ticks()

            
        # produce polar plots to get handles for the colorbars
        xgrid = plot_options_dic['xgrid']
        ygrid = plot_options_dic['ygrid']

        if Ncontours is None:
            polar = ax.pcolormesh(xgrid, ygrid, qty0, cmap=cscheme)
        else:
            polar = ax.contourf(xgrid,ygrid,qty0,levels,cmap=cscheme,extend='both') 

        # set data/positioning of colorbar
        if VERTICAL_CBAR:
            
            cbar = plt.colorbar(polar,ax=ax,pad=0.0,extend='both')
            
            # set colorbar limits and ticks
            cbar.set_clim(np.min(levels),np.max(levels)) 
            cbar.set_ticks(ticks)
            cbar.set_ticklabels(ticks)
            var0 = ''.join([i for i in vars[0] if not i.isdigit()])
            cbar.set_label(label_dic[var0])
            cbar.update_normal(polar)

            if key is '1':
                cbar.remove()

            if key is '2':# fix positioning of right axis and cbar_sim2
                bbox_cbar = cbar.ax.get_position()
                pts_cbar = bbox_cbar.get_points()
                width_cbar = pts_cbar[1][0] - pts_cbar[0][0]
                height_cbar = pts_cbar[1][1] - pts_cbar[0][1]
                new_width = 0.4*width_cbar
                cbar.ax.set_position([pts_cbar[0][0]-1.3*new_width,pts_cbar[0][1],new_width,height_cbar])

    # adjust width of zoom-region
    axR.set_position([ptsL[1][0]+0.01*gap,ptsL[0][1],0.45*heightL,heightL])

    # Create a Rectangle patch
    box_width = SETTINGS_L['box_width'] #1e3
    rect = patches.Rectangle((0,-box_width),box_width,2*box_width,linewidth=1,edgecolor='k',facecolor='none')
    axL.add_patch(rect)

    # draw lines connecting rectangle to right panel to indicate zoom-region
    ymax = SETTINGS_L['ymax'] #8e3
    yp = 0.5 + 0.85*box_width/(2*ymax) #ptsR[1][1]
    ym = 0.5 - 0.85*box_width/(2*ymax) #ptsL[1][0]-gap
    l1 = lines.Line2D([0.525*heightR, ptsL[1][0]], [yp, 0.5+0.5*heightR], color='k', linewidth=1, transform=fig.transFigure, figure=fig)
    l2 = lines.Line2D([0.525*heightR, ptsL[1][0]], [ym, 0.5-0.5*heightR], color='k', linewidth=1, transform=fig.transFigure, figure=fig)
    fig.lines.extend([l1, l2])

    return fig,axL,axR,cbar,levels 


def explorer6(plot_options_dic, dump_dirs, GridDic, VarDic, LogDic, CmapDic):
    
    fig = figure(figsize=(12.5, 7.75))
    matplotlib.rcParams['font.size'] = '9'
    plt.subplots_adjust(left=0.18, bottom=0.06, right=0.9, top=0.97, wspace=None, hspace=None)

    AxDic = {}

    gs1a = gridspec.GridSpec(4, 2)
    gs1a.update(left=0.06, right=0.33, hspace=0.25)
    AxDic['1'] = plt.subplot(gs1a[0:-2, :])
    AxDic['1'].grid()

    gs1b = gridspec.GridSpec(7, 2)
    gs1b.update(left=0.06, right=0.33, bottom = 0.06, hspace=0.15)
    AxDic['2']= plt.subplot(gs1b[-3:-2, :])
    AxDic['3'] = plt.subplot(gs1b[-2:-1, :])
    AxDic['4'] = plt.subplot(gs1b[-1:, :])

    gs2 = gridspec.GridSpec(4, 3)
    gs2.update(left=0.4, right=0.98, hspace=0.15)

    AxDic['5a'] = plt.subplot(gs2[0:2, 0])
    AxDic['5b'] = plt.subplot(gs2[0:2, 1])
    AxDic['5c'] = plt.subplot(gs2[0:2, -1])

    AxDic['6a'] = plt.subplot(gs2[2:, 0])
    AxDic['6b'] = plt.subplot(gs2[2:, 1])
    AxDic['6c'] = plt.subplot(gs2[2:, -1])

    # hide various x-axes 
    keys = ['2','3','5a','5b','5c']
    for key in keys:
        ax = AxDic[key]
        ax.get_xaxis().set_visible(False) 

    # hide various y-axes 
    keys = ['5b','5c','6b','6c']
    for key in keys:
        ax = AxDic[key]
        ax.get_yaxis().set_visible(False) 

    # hide top y-tick on axis 3
    ticks3 = AxDic['3'].get_yticklabels()
    ticks3[-1].set_visible(False)

    # set zoom-in region 
    i_zoom = GridDic['1']['i_zoom']
    r_c = GridDic['1']['r_c']
    t_c = GridDic['1']['theta_c']
    keys = CmapDic.keys()
    for key in keys:
        ax = AxDic[key]
        ax.axis([0.,r_c[i_zoom], -r_c[i_zoom], r_c[i_zoom]])
        if plot_options_dic['RAW_GRID_DATA']:
            ax.axis([0,i_zoom-1, len(t_c)-1, 0])
        # else:
        #     #enforce same y-ticks as x-ticks
        #     xticks = ax.get_xticks()
        #     ax.set_yticks(xticks)


    # add titles 
    # suptitle(fig_title)
    # AxDic[key].set_title(plot_titles[0])

    # xlabels
    keys = ['6a','6b','6c']
    for key in keys:
        ax = AxDic[key]
        if plot_options_dic['RAW_GRID_DATA']:
            ax.set_xlabel(r'$i$')
        else:
            ax.set_xlabel(r'$r$')

    # ylabels
    keys = ['5a','6a']
    for key in keys:
        ax = AxDic[key]
        if plot_options_dic['RAW_GRID_DATA']:
            ax.set_ylabel(r'$j$')
        else:
            ax.set_ylabel(r'$z$')

    # dics of all plot labels 
    LabelDic_lin = sim_dics.load_labels(0)
    LabelDic_log = sim_dics.load_labels(1)

    # load all variables to plot
    vars = []
    for key in VarDic.keys():
        vars.append(VarDic[key])
    i_cbar = plot_options_dic['i_cbar']
    i_dump1 = plot_options_dic['i_dumps'][0]
    SimDic = sim_dics.load_vars(i_cbar,vars,dump_dirs,GridDic)
    SimDic0 = sim_dics.load_vars(i_dump1,vars,dump_dirs,GridDic)

    # Colormap plots
    CbarDic = {}
    LevelDic = {}
    x_grid,y_grid = plot_options_dic['x_grid'],plot_options_dic['y_grid']
    for key in CmapDic.keys():
        qty = SimDic['1'][VarDic[key]]
        qty0 = SimDic0['1'][VarDic[key]]
        qty_min,qty_max = np.min(qty),np.max(qty)
        #print "[explorer12]: key = {}, qty = {}".format(key,qty)
        pos_vals = np.where(qty > 0.)
        qty_pos_min = np.min(qty[pos_vals])
        qty_pos_max = np.max(qty[pos_vals])
        # qty_pos_min, qty_pos_max = qty_min,qty_max

        # zero_vals = np.where(qty == 0.)

        # take log?
        if LogDic[key]:
            qty_pos_min = np.log10(qty_pos_min)
            qty_pos_max = np.log10(qty_pos_max)

            if qty_min < 0.:
                qty = np.log10(np.abs(qty))
            else:
                qty = np.log10(qty)

            if np.min(qty0) < 0.:
                qty0 = np.log10(np.abs(qty0))
            else:
                qty0 = np.log10(qty0)


        # redirect d_floor and p_floor
        if VarDic[key] is 'd_floor':
            qty = np.log10(SimDic['1']['d'])
        if VarDic[key] is 'p_floor':
            qty = np.log10(SimDic['1']['p'])

        # figure out the limits
        if LogDic[key] or VarDic[key] is 's':
            imin,imax = qty_pos_min, qty_pos_max 
        else:
            imin,imax = qty_min,qty_max

        print "key = {}, LOG({}): imin,imax = {},{}".format(key,VarDic[key],imin,imax)
        if imin != imin:
            imin,imax = qty_pos_min,qty_pos_max
        Ncontours = plot_options_dic['Ncontours']
        if LogDic[key] or VarDic[key] is 's':
            print "key = {}, LOG({}): before - imin,imax = {},{}".format(key,VarDic[key],imin,imax)
            imin,imax = np.round(imin),np.round(imax)
            if imin == imax:
                imax += 1
            if Ncontours is None:
                Nlevels = (imax-imin)*100+1
            else:
                Nlevels = Ncontours
            print "key = {}, LOG({}): after - imin,imax = {},{}".format(key,VarDic[key],imin,imax)
            Nticks = (imax-imin)+1
            if Nticks <= 5:
                ticks = np.arange(imin,imax+1,0.5)
            else:
                ticks = np.arange(imin,imax+1,1.)
            if len(ticks) > 10:
                Nskip = ceil(len(ticks)/10.)
            else:
                Nskip = 1
            ticks = ticks[::Nskip]
            levels = np.linspace(imin,imax,Nlevels)
        else:
            print "key = {}, LIN({}): imin,imax = {},{}".format(key,VarDic[key],imin,imax)
            if Ncontours is None:
                Nlevels = 100
            else:
                Nlevels = Ncontours
            Nticks = 12
            if imin < 0. and abs(max(imin,imax))/abs(min(imin,imax)) < 5.:
                imax = max(abs(imin),abs(imax))
                imin = -imax
            levels = np.linspace(imin,imax,Nlevels)
            ticks = tick_function(levels,Nticks+1)
        
        
        print "ticks = {}, levels.shape = {}".format(ticks,levels.shape)

        # plot
        ax = AxDic[key]
        if Ncontours is None:
            CbarDic[key] = ax.pcolormesh(x_grid, y_grid, qty0, cmap=CmapDic[key])
        else:
            CbarDic[key] = contourf(x_grid,y_grid,qty0,levels,cmap=CmapDic[key],extend='both')

        # add colorbars; set cbar limits and ticks
        cbar = plt.colorbar(CbarDic[key],ax=ax,pad=0.02,extend='both')
        cbar.set_clim(np.min(levels),np.max(levels)) 
        cbar.set_ticks(ticks)
        cbar.set_ticklabels(ticks)
        var_name = ''.join([i for i in VarDic[key] if not i.isdigit()])
        # place titles for each cmap at colorbar
        # if LogDic[key]:
        #     cbar.set_label(LabelDic_log[var_name])
        # else:
        #     cbar.set_label(LabelDic_lin[var_name])
        cbar.update_normal(CbarDic[key])
        CbarDic[key + '_cbar'] = cbar
        LevelDic[key] = levels

        # place titles for each cmap above
        if LogDic[key]:
            ax.set_title(LabelDic_log[var_name])
        else:
            ax.set_title(LabelDic_lin[var_name])


    return fig,AxDic,LevelDic

def explorer12(plot_options_dic, dump_dirs, GridDic, VarDic, LogDic, CmapDic):
    
    fig = figure(figsize=(12.5, 7.75))
    matplotlib.rcParams['font.size'] = '9'
    plt.subplots_adjust(left=0.18, bottom=0.06, right=0.9, top=0.97, wspace=None, hspace=None)

    AxDic = {}

    # gs1 = gridspec.GridSpec(4, 2)
    # gs1.update(left=0.06, right=0.33, wspace=0.05, hspace=0.25)
    # AxDic['1'] = plt.subplot(gs1[:2, :])
    # AxDic['2']= plt.subplot(gs1[-2, :])
    # AxDic['3'] = plt.subplot(gs1[-1, :])

    gs1a = gridspec.GridSpec(4, 2)
    gs1a.update(left=0.06, right=0.33, hspace=0.25)
    AxDic['1'] = plt.subplot(gs1a[0:-2, :])
    AxDic['1'].grid()

    # gs1b = gridspec.GridSpec(5, 2)
    # gs1b.update(left=0.06, right=0.33, bottom = 0.075, hspace=0.05)
    # AxDic['2']= plt.subplot(gs1b[-2:-1, :])
    # AxDic['3'] = plt.subplot(gs1b[-1:, :])

    gs1b = gridspec.GridSpec(7, 2)
    gs1b.update(left=0.06, right=0.33, bottom = 0.06, hspace=0.15)
    AxDic['2']= plt.subplot(gs1b[-3:-2, :])
    AxDic['3'] = plt.subplot(gs1b[-2:-1, :])
    AxDic['4'] = plt.subplot(gs1b[-1:, :])

    gs2 = gridspec.GridSpec(4, 3)
    gs2.update(left=0.4, right=0.98, hspace=0.15)

    AxDic['4a'] = plt.subplot(gs2[0, 0])
    AxDic['4b'] = plt.subplot(gs2[0, 1])
    AxDic['4c'] = plt.subplot(gs2[0, -1]) 

    AxDic['5a'] = plt.subplot(gs2[1, 0])
    AxDic['5b'] = plt.subplot(gs2[1, 1])
    AxDic['5c'] = plt.subplot(gs2[1, -1])

    AxDic['6a'] = plt.subplot(gs2[2, 0])
    AxDic['6b'] = plt.subplot(gs2[2, 1])
    AxDic['6c'] = plt.subplot(gs2[2, -1])

    AxDic['7a'] = plt.subplot(gs2[3, 0])
    AxDic['7b'] = plt.subplot(gs2[3, 1])
    AxDic['7c'] = plt.subplot(gs2[3, -1])

    # hide various x-axes 
    keys = ['2','3','4a','4b','4c','5a','5b','5c','6a','6b','6c']
    for key in keys:
        ax = AxDic[key]
        ax.get_xaxis().set_visible(False) 

    # hide various y-axes 
    keys = ['4b','4c','5b','5c','6b','6c','7b','7c']
    for key in keys:
        ax = AxDic[key]
        ax.get_yaxis().set_visible(False) 

    # hide top y-tick on axis 3
    ticks3 = AxDic['3'].get_yticklabels()
    ticks3[-1].set_visible(False)

    # set zoom-in region 
    i_zoom = GridDic['1']['i_zoom']
    r_c = GridDic['1']['r_c']
    t_c = GridDic['1']['theta_c']
    keys = CmapDic.keys()
    for key in keys:
        ax = AxDic[key]
        ax.axis([0.,r_c[i_zoom], 0., r_c[i_zoom]])
        if plot_options_dic['RAW_GRID_DATA']:
            ax.axis([0,i_zoom-1, len(t_c)-1, 0])
        # else:
        #     #enforce same y-ticks as x-ticks
        #     xticks = ax.get_xticks()
        #     ax.set_yticks(xticks)


    # add titles 
    # suptitle(fig_title)
    # AxDic[key].set_title(plot_titles[0])

    # xlabels
    keys = ['7a','7b','7c']
    for key in keys:
        ax = AxDic[key]
        if plot_options_dic['RAW_GRID_DATA']:
            ax.set_xlabel(r'$i$')
        else:
            ax.set_xlabel(r'$r$')

    # ylabels
    keys = ['4a','5a','6a','7a']
    for key in keys:
        ax = AxDic[key]
        if plot_options_dic['RAW_GRID_DATA']:
            ax.set_ylabel(r'$j$')
        else:
            ax.set_ylabel(r'$z$')

    # dics of all plot labels 
    LabelDic_lin = sim_dics.load_labels(0)
    LabelDic_log = sim_dics.load_labels(1)

    # load all variables to plot
    vars = []
    for key in VarDic.keys():
        vars.append(VarDic[key])
    i_cbar = plot_options_dic['i_cbar']
    i_dump1 = plot_options_dic['i_dumps'][0]
    SimDic = sim_dics.load_vars(i_cbar,vars,dump_dirs,GridDic)
    SimDic0 = sim_dics.load_vars(i_dump1,vars,dump_dirs,GridDic)

    # Colormap plots
    CbarDic = {}
    LevelDic = {}
    x_grid,y_grid = plot_options_dic['x_grid'],plot_options_dic['y_grid']
    for key in CmapDic.keys():
        qty = SimDic['1'][VarDic[key]]
        qty0 = SimDic0['1'][VarDic[key]]
        qty_min,qty_max = np.min(qty),np.max(qty)
        #print "[explorer12]: key = {}, qty = {}".format(key,qty)
        pos_vals = np.where(qty > 0.)
        qty_pos_min = np.min(qty[pos_vals])
        qty_pos_max = np.max(qty[pos_vals])
        # qty_pos_min, qty_pos_max = qty_min,qty_max

        # zero_vals = np.where(qty == 0.)

        # take log?
        if LogDic[key]:
            qty_pos_min = np.log10(qty_pos_min)
            qty_pos_max = np.log10(qty_pos_max)

            if qty_min < 0.:
                qty = np.log10(np.abs(qty))
            else:
                qty = np.log10(qty)

            if np.min(qty0) < 0.:
                qty0 = np.log10(np.abs(qty0))
            else:
                qty0 = np.log10(qty0)


        # redirect d_floor and p_floor
        if VarDic[key] is 'd_floor':
            qty = np.log10(SimDic['1']['d'])
        if VarDic[key] is 'p_floor':
            qty = np.log10(SimDic['1']['p'])

        # figure out the limits
        imin,imax = qty_pos_min, qty_pos_max #qty_min,qty_max
        print "key = {}, LOG({}): imin,imax = {},{}".format(key,VarDic[key],imin,imax)
        if imin != imin:
            imin,imax = qty_pos_min,qty_pos_max
        Ncontours = plot_options_dic['Ncontours']
        if LogDic[key] or VarDic[key] is 's':
            print "key = {}, LOG({}): before - imin,imax = {},{}".format(key,VarDic[key],imin,imax)
            imin,imax = np.round(imin),np.round(imax)
            if imin == imax:
                imax += 1
            if Ncontours is None:
                Nlevels = (imax-imin)*100+1
            else:
                Nlevels = Ncontours
            print "key = {}, LOG({}): after - imin,imax = {},{}".format(key,VarDic[key],imin,imax)
            Nticks = (imax-imin)+1
            if Nticks <= 5:
                ticks = np.arange(imin,imax+1,0.5)
            else:
                ticks = np.arange(imin,imax+1,1.)
            if len(ticks) > 10:
                Nskip = ceil(len(ticks)/10.)
            else:
                Nskip = 1
            ticks = ticks[::Nskip]
            levels = np.linspace(imin,imax,Nlevels)
        else:
            print "key = {}, LIN({}): imin,imax = {},{}".format(key,VarDic[key],imin,imax)
            if Ncontours is None:
                Nlevels = 100
            else:
                Nlevels = Ncontours
            Nticks = 12
            if imin < 0. and abs(max(imin,imax))/abs(min(imin,imax)) < 5.:
                imax = max(abs(imin),abs(imax))
                imin = -imax
            levels = np.linspace(imin,imax,Nlevels)
            ticks = tick_function(levels,Nticks+1)
        
        
        print "ticks = {}, levels.shape = {}".format(ticks,levels.shape)

        # plot
        ax = AxDic[key]
        if Ncontours is None:
            CbarDic[key] = ax.pcolormesh(x_grid, y_grid, qty0, cmap=CmapDic[key])
        else:
            CbarDic[key] = contourf(x_grid,y_grid,qty0,levels,cmap=CmapDic[key],extend='both')

        # add colorbars; set cbar limits and ticks
        cbar = plt.colorbar(CbarDic[key],ax=ax,pad=0.02,extend='both')
        cbar.set_clim(np.min(levels),np.max(levels)) 
        cbar.set_ticks(ticks)
        cbar.set_ticklabels(ticks)
        var_name = ''.join([i for i in VarDic[key] if not i.isdigit()])
        # place titles for each cmap at colorbar
        # if LogDic[key]:
        #     cbar.set_label(LabelDic_log[var_name])
        # else:
        #     cbar.set_label(LabelDic_lin[var_name])
        cbar.update_normal(CbarDic[key])
        CbarDic[key + '_cbar'] = cbar
        LevelDic[key] = levels

        # place titles for each cmap above
        if LogDic[key]:
            ax.set_title(LabelDic_log[var_name])
        else:
            ax.set_title(LabelDic_lin[var_name])


    return fig,AxDic,LevelDic