import numpy as np
"""
This file is used to calculate the physical units and
set corresponding code units of the included simulation
dataset (data_JHcontest2019.tar)

These parameters come from this publication:
https://ui.adsabs.harvard.edu/abs/2013ApJ...767..156M/abstract
"""

# physical constants
Fund = {'mp':1.6726231e-24, 'me':9.1093898e-28, 'c':3.0e10, 'h':6.6260755e-27, \
				'e':4.8032068e-10, 'kb':1.380658e-16, 'G':6.67259e-8}
ThomsonCrossSection = (8.*np.pi/3.)*(Fund['e']**2/(Fund['me']*Fund['c']**2))**2
Fund['sig_th'] = ThomsonCrossSection

#---------------------------------------------------------------------
# inputs
#---------------------------------------------------------------------
FIND_EQUILB = 1
xfac = 1. 				# extend the outer boundary

# specify (or determine) domain size (in units of r_B)
if 0:
	r_out = 2.
	r_in = r_out/8e3 #6.61e-4
elif 0:
	r_out = 1.3
	r_in = r_out/1e3/xfac
elif 1:
	r_out = 1.3*xfac
	r_in = 1.3/1e3
else:
	r_out = 1.3
	r_in = 6.61e-4

Nr = 400
x1rat = (r_out/r_in)**(1./Nr)

# mus
mu = 0.6  #0.62
mu_e = 1. #1.18
mu_H = 1. #1.43
mbar = mu*Fund['mp']
Msun = 1.989e33
M = 1e8*Msun
L_edd = 4.*np.pi*Fund['G']*M*Fund['mp']*Fund['c']/Fund['sig_th']
R_s = 2.*Fund['G']*M/Fund['c']**2
r_out_cgs = 6.9683e6*8.84e13*xfac

Gamma = 5./3.
gfac = 1./(Gamma*(Gamma-1.))

# f_X = 5e-4
# f_X = 0.008
# f_X = 0.01
f_X = 0.015
# f_X = 0.03 

# specify Eddington luminosity
L_X = L_edd*f_X

# specify/compute Bondi radius
r_B = r_out_cgs/R_s/r_out

c0 = Fund['c']/np.sqrt(2.*r_B)
T0 = (c0**2/Gamma)*mbar/Fund['kb']
p0 = 0.5/Gamma/r_B

# specify/determine density and ionization parameter at r_out
rho0 = 1e-23
n0 = rho0/mbar
xi0 = 60.
if FIND_EQUILB:
	xi0 = (L_X/n0/R_s**2)/(r_out*r_B)**2
	
# Luminosity units
L0 = R_s*n0/Fund['mp']/Fund['c']**3*mu/mu_e/mu_H

# numerical floors
d_floor = 0.1
p_floor = 0.02*p0/xfac**1.5

# Conversions to cgs units 
d_units = n0
v_units = Fund['c']
p_units = rho0*Fund['c']**2
T_units = Fund['c']**2*mbar/Fund['kb']
xi_units = L_X/n0/R_s**2
t_units = R_s/Fund['c']

t_dyn = r_B**1.5/np.sqrt(2.)
t_ff = (r_out*r_B)**1.5
vkep_i = np.sqrt(Fund['G']*M/(r_in*R_s))
vkep_o = np.sqrt(Fund['G']*M/(r_out*R_s))
t_orbit_i = 2.*np.pi*r_in*R_s/vkep_i
t_orbit_o = 2.*np.pi*r_out*R_s/vkep_o

# output interval
dt = 0.01*t_dyn

# Athena++ input file parameters
#===============================================
print "\ninput file: <output1>:\n"
print "/***********************************/"
print "dt = " + str(dt)
print "/***********************************/\n"
print "\ninput file: <output2>:\n"
print "/***********************************/"
print "dt = " + str(dt)
print "/***********************************/\n"
print "\ninput file: <output3>:\n"
print "/***********************************/"
print "dt = " + str(20*dt)
print "/***********************************/\n\n"

print "\ninput file: <time>:\n"
print "/***********************************/"
print "tlim = " + str(50*t_dyn)
print "/***********************************/\n\n"

print "\ninput file: <mesh>:\n"
print "/***********************************/"
print "x1min = " + str(r_in*r_B)
print "x1max = " + str(r_out*r_B)
print "x1rat = " + str(x1rat)
print "/***********************************/\n\n"

print "\ninput file: <problem>:\n"
print "/***********************************/"
print "lambda_0 = " + str(r_B)
print "xi0 = " + str(xi_units)
print "T0 = " + str(T_units)
print "hc_norm = " + str(L0)
print "rho_manual = " + str(xi_units/xi0/(r_out*r_B)**2)
print "/***********************************/\n\n"

print "\ninput file: <hydro>:\n"
print "/***********************************/"
print "dfloor = " + str(d_floor)
print "pfloor = " + str(p_floor)
print "/***********************************/\n\n"