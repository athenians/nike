import athena_read   # Athena++'s reader
import numpy as np
import sys
import code_units as units; reload(units)


def my_var(grid_dic,dump_dir,d,p,v_r,v_t,v_p,B_r=None,B_t=None,B_p=None):
	# T_vir = 3.*grid_dic['r_c_grid']*p/d
	gamma = 5./3.
	# difference variable - the variable minus its theta-average
	# var = p/d**gamma
	var = d
	# v = np.sqrt(v_r**2 + v_t**2)
	# cs2 = gamma*p/d
	# var = qty = 0.5*v**2 + cs2/(gamma - 1.) - \
	# 					units.GM/grid_dic['r_c_grid'] 
	if 0: # compare with analytic bondi solution
		r_B = 1e3
		Gamma = gamma
		filepath = '../grizzly/MP08J/T_BONDIsoln1of1(inflow)_HEP1000_g1.66667_at7.84793(PW).tab'
		Mdot_bondi = 0.25*((5.-3.*Gamma)/2.)**(-(5.-3.*Gamma)/2./(Gamma-1.))*r_B**1.5/np.sqrt(2.)
		rs_bondi = 0.25*(5.-3.*Gamma)
		Delta = (5.-3.*Gamma + 8./r_B)**2 - 64.*(1. - Gamma + 1./r_B)/r_B
		rs_bondiPW = 0.5*rs_bondi + 1./r_B + np.sqrt(Delta)/8.

		data = np.loadtxt(filepath,skiprows=1)
		x_exact = data[:,0]
		r_exact = 1./r_B + x_exact
		p_exact = data[:,4] 
		d_exact = data[:,3]
		v_exact = data[:,2] 
		cs_exact = np.sqrt(Gamma*p_exact/d_exact)
		M_exact = data[:,1] 
		s_exact = np.log(p_exact/d_exact**Gamma)
		Mdot_exact = d_exact*v_exact*(r_exact*r_B)**2
		Be_exact = cs_exact**2/(Gamma-1.) - 0.5/r_exact + 0.5*v_exact**2

		var_exact = Be_exact

	if 1:
		dump_file = dump_dir + str(1).zfill(5) + '.athdf' 
		athena_dic = athena_read.athdf(dump_file)

		k = 0
		i_min = grid_dic['i_min']
		i_max = grid_dic['i_max']
		j_min = grid_dic['j_min']
		j_max = grid_dic['j_max']
		k_min = grid_dic['k_min']
		k_max = grid_dic['k_max']
		
		# unpack primitive variables
		d = athena_dic['rho'][k,j_min:j_max,i_min:i_max]
		p = athena_dic['press'][k,j_min:j_max,i_min:i_max]
		v_r = athena_dic['vel1'][k,j_min:j_max,i_min:i_max]
		v_t = athena_dic['vel2'][k,j_min:j_max,i_min:i_max]
		v_p = athena_dic['vel3'][k,j_min:j_max,i_min:i_max]	

		var1 = d
		var_diff = var - var1

	# Nr = len(var[0,:])
	# var_theta_avg = np.zeros(Nr)
	# var_diff = np.zeros_like(var)
	# for i in range(Nr):
	# 	var_theta_avg[i] = np.mean(var[:,i])
	# 	var_diff[:,i] = var[:,i] - var_theta_avg[i]
		# var_diff[:,i] = 100.*(var[:,i] - var_theta_avg[i])/var_theta_avg[i]
		# var_diff[:,i] = 100.*(var[:,i] - var_exact[i])/var_exact[i]
		# print i,var_exact[i],np.max(var_diff[:,i])
	return np.abs(var_diff) + 1e-4

def my_var_label(logval):
	if logval:
		return r'$\log{\rho - \rho(t=0.5)}$'
	else:
		return r'$\rho - \rho(t=0.5)$'


def uov_scalar(qty,grid_dic,d,p,v_r,v_t,v_p,B_r,B_t,B_p):
	""" 
	This function is to be defined by the user to 
	perform any manipulations of scalar user output 
	variables. The function uov_vector() calls this 
	function upon loading each uov.

	Below is an example of performing some 
	manipulations assuming index=0 and qty=div(B).  
	"""

	if 0: # div(B) calc - change to 0 to ignore
	# For analyzing divergence of B, we likely want
	# to take a log, so here we set all zero values to 
	# a small number.  We also normalize div(B) by B.
		filter = np.where(qty == 0.)
		qty[filter] = 1e-25

		# normalize div B by B
		B = np.sqrt(B_r**2 + B_t**2 + B_p**2)
		qty = qty #/B
	
	else: # user can add modify as needed 
		qty = qty

	return qty


def uov_vector(uovs,grid_dic,d,p,v_r,v_t,v_p,B_r=None,B_t=None,B_p=None):
	""" 
	This function is to be defined by the user to 
	perform any manipulations of vector user output 
	variables. The function load_vars() calls this 
	function after adding all vector components 
	(which are individual uov's) to an array, which 
	is passed here as the input "uovs".

	A scalar uov will be passed along to uov_scalar()

	Currently this function is set to expect the
	components of current density, j = curl(B), in
	spherical coordinates.  The (r,theta,phi) 
	components of j correspond to indices (0,1,2).
	Below we further take the cross-product with
	B to arrive at the Lorentz force,f_L = jxB.  
	"""

	N = len(uovs)
	print "[uov_vector]: N = {}".format(N)
	j = {}
	for n in range(N):
		key = `n+1`
		j[key] = np.array(uovs[n])

	if N is 1: # not a vector; call scalar routine instead
		qty = uov_scalar(uovs[0],grid_dic,d,p,v_r,v_t,v_p,B_r,B_t,B_p)
		return [qty]
	
	# now we compute jxB
	elif N is 2:
		j_r,j_t = j['1'],j['2']
		jcrossB_p = j_r*B_t - j_t*B_r
		return [jcrossB_p]

	elif N is 3:
		j_r,j_t,j_p = j['1'],j['2'],j['3']
		jcrossB_r = j_t*B_p - j_p*B_t
		jcrossB_t = j_r*B_p - j_p*B_r
		jcrossB_p = j_r*B_t - j_t*B_r
		return [jcrossB_r, jcrossB_t, jcrossB_p]
# 		return np.sqrt(j_r**2 + j_t**2 + j_p**2)
	
	# user can add modify as needed 
	else: # if N > 3 
		uov_array = []
		for key in j.keys():
			uov_array.append(j[key])
		return uov_array



def user_out_var_label(logval):
	if logval:
		return r'$j$' #r'$\log{((\nabla \cdot B)/B)}$'
	else:
		return r'$div(B)/B$'


def dump_dir_dic(run_names,run_paths,dump_tags,uov_tags=None):
	"""
	inuts:
	run_names - always an array of strings specifying the dump folder
	run_paths - either a string or a dictionary of strings
			  - the strings are names of the paths to the dump folder
	dump_tags - either a string or a dictionary of strings
			  - the strings are names of the actual output dumps
	uov_tags  - (optional) either a string or a dictionary of strings
			  - dump_tags of "user output variables" (see Athena++ docs)

	output:
	dump_dirs - a dictionary containing the complete paths to individual dumps
				for all specified simulations 
	"""
	dump_dirs = {}
	
	Nsims = len(run_names)
	
	# paths to all the sim directories 
	if isinstance(run_paths, dict):
		for n in range(1,Nsims+1):
			key = `n`
			if key in run_paths.keys():
				dump_dirs[key+'path'] = run_paths[key] + run_names[n-1] + '/'
			else:
				dump_dirs[key+'path'] = run_paths['1'] + run_names[n-1] + '/'
				print "[dump_dir_dic]: n = {}, path = {}".format(n,dump_dirs[key+'path'])
	else: # run_paths is a string
		for n in range(1,Nsims+1):
			dump_dirs[`n`+'path'] = run_paths + run_names[n-1] + '/'
	
	# path to the main data files
	if isinstance(dump_tags, dict):
		for n in range(1,Nsims+1):
			key = `n`
			if key in dump_tags.keys():
				dump_dirs[key] = dump_dirs[key+'path'] + dump_tags[key]
			else:
				dump_dirs[key] = dump_dirs[key+'path'] + dump_tags['1']
	else: # dump_tags is a string not a dic
		for n in range(1,Nsims+1):
			dump_dirs[`n`] = dump_dirs[`n`+'path'] + dump_tags

	# path to user output data files
	if uov_tags is not None: 
		if isinstance(uov_tags, dict):
			for n in range(1,Nsims+1):
				key = `n` + 'uov'
				if `n` in dump_tags.keys():
					dump_dirs[key] = dump_dirs[`n`+'path'] + uov_tags[`n`]
				else:
					dump_dirs[key] = dump_dirs[`n`+'path'] + uov_tags['1']
		else: # uov_tags is a string not a dic
			for n in range(1,Nsims+1):
				dump_dirs[`n`+'uov'] = dump_dirs[`n`+'path'] + uov_tags
			
	return dump_dirs


def load_labels(logval=False):
	""" Returns a dictionary of latex plot labels for all variables 
		currently implemented
	
	Inputs:
	logval - if True, returns r'$\log(qty)$' instead of just r'$qty$'

	Example usage:
	vars = ['B']
	SimDic = load_vars(i_dump,vars,dump_dirs,grid_dics)
	LabelDic = load_labels(logval)
	plot(r_c,qty,label=LabelDic['B'])
	"""
	
	LabelDic = {}	
	hydro_vars = {}
	misc_vars = {}
	mhd_vars = {}
	
	if logval:
		# labels for primitive hydro variables 
		hydro_vars['d'] = r'$\log(n$' + ' ' + r'$[\rm{cm}^{-3}])$'
		hydro_vars['p'] = r'$\log(p)$'
		hydro_vars['v_r'] = r'$\log(v_r)$'
		hydro_vars['v_t'] = r'$\log(v_t)$'
		hydro_vars['v_p'] = r'$\log(v_\phi/v_{kep})$'
		
		# labels for derived hydro variables 
		hydro_vars['v'] = r'$\log(v)$'
		hydro_vars['vp'] = r'$\log(v_{poloidal})$'
		hydro_vars['cs'] = r'$\log(c_s)$'
		hydro_vars['c_iso'] = r'$\log(c_{iso})$'
		hydro_vars['s'] = r'$s=\log(s)$' 
		hydro_vars['T'] = r'$\log(T/T_C)$'
		hydro_vars['T_vir'] = r'$\log(T_{vir})$'
		hydro_vars['M'] = r'$\log(M)$'
		hydro_vars['Mp'] = r'$\log(M_{poloidal})$'
		hydro_vars['l_z'] = r'$\log(l_z)$'
		hydro_vars['l_p'] = r'$\log(l_p)$'
		hydro_vars['l'] = r'$\log(l)$'
		hydro_vars['Be'] = r'$\log(Be)$'
		hydro_vars['xi'] = r'$\log(\xi)$'
		hydro_vars['w_r'] = r'$\log[(\nabla\times v)_r]$'
		hydro_vars['w_t'] = r'$\log[(\nabla\times v)_t]$'
		hydro_vars['w_p'] = r'$\log[(\nabla\times v)_p]$'
		hydro_vars['wp'] = r'$\log[(\nabla\times v)_{poloidal}]$'
		hydro_vars['w'] = r'$\log[(\nabla\times v)]$'

		# labels for MHD quantities
		mhd_vars['B_r'] = r'$\log(B_r)$'
		mhd_vars['B_t'] = r'$\log(B_\theta)$'
		mhd_vars['B_p'] = r'$\log(B_\phi)$'
		mhd_vars['B'] = r'$\log(B)$'
		mhd_vars['Bp'] = r'$\log(B_{poloidal})$'
		mhd_vars['beta'] = r'$\log(\beta)$'
		mhd_vars['v_A'] = r'$\log(v_{A})$'
		mhd_vars['Bphi_over_Bp'] = r'$\log(B_\phi/B_{poloidal})$'
		mhd_vars['B2'] = r'$\log{(B^2)}$'

		# label for my_var, user_out_var, & floor maps
		misc_vars['my_var'] = my_var_label(logval)
		misc_vars['user_out_var'] = user_out_var_label(logval)
		misc_vars['d_floor'] = r'$\rho_{floor}$' + ' triggers'
		misc_vars['p_floor'] = r'$p_{floor}$' + ' triggers'
		misc_vars['dt_cs_r'] = r'$\log(dt_{cs,r})$'
		misc_vars['dt_cs_t'] = r'$\log(dt_{cs,\theta})$'
		misc_vars['dt_v_r'] = r'$\log(dt_{v,r})$'
		misc_vars['dt_v_t'] = r'$\log(dt_{v,\theta})$'
		misc_vars['dt_vA_r'] = r'$\log(dt_{v_A,r})$'
		misc_vars['dt_vA_t'] = r'$\log(dt_{v_A,\theta})$'

	else:
		# labels for primitive hydro variables 
		hydro_vars['d'] = r'$n$' + ' ' + r'$[\rm{cm}^{-3}])$'
		hydro_vars['p'] = r'$p$'
		hydro_vars['v_r'] = r'$v_r$'
		hydro_vars['v_t'] = r'$v_t$'
		hydro_vars['v_p'] = r'$v_\phi/v_{kep}$'
		
		# labels for derived hydro variables 
		hydro_vars['v'] = r'$v$'
		hydro_vars['vp'] = r'$v_{poloidal}$'
		hydro_vars['cs'] = r'$c_s$'
		hydro_vars['c_iso'] = r'$c_{iso}$'
		hydro_vars['s'] = r'$s=\log(p/\rho^\gamma)$' 
		hydro_vars['T'] = r'$T/T_C$'
		hydro_vars['T_vir'] = r'$T_{vir}$'
		hydro_vars['M'] = r'$M$'
		hydro_vars['Mp'] = r'$M_{poloidal}$'
		hydro_vars['l_z'] = r'$l_z$'
		hydro_vars['l_p'] = r'$l_p$'
		hydro_vars['l'] = r'$l$'
		hydro_vars['Be'] = r'$Be$'
		hydro_vars['xi'] = r'$\xi$'
		hydro_vars['w_r'] = r'$(\nabla\times v)_r$'
		hydro_vars['w_t'] = r'$(\nabla\times v)_t$'
		hydro_vars['w_p'] = r'$(\nabla\times v)_p$'
		hydro_vars['wp'] = r'$(\nabla\times v)_{poloidal}$'
		hydro_vars['w'] = r'$(\nabla\times v)$'

		# labels for MHD quantities
		mhd_vars['B_r'] = r'$B_r$'
		mhd_vars['B_t'] = r'$B_\theta$'
		mhd_vars['B_p'] = r'$B_\phi$'
		mhd_vars['B'] = r'$B$'
		mhd_vars['Bp'] = r'$B_{poloidal}$'
		mhd_vars['beta'] = r'$\beta$'
		mhd_vars['v_A'] = r'$v_{A}$'
		mhd_vars['Bphi_over_Bp'] = r'$B_\phi/B_{poloidal}$'
		mhd_vars['B2'] = r'$B^2$'

		# label for my_var, user_out_var, floor maps, & dt maps
		misc_vars['my_var'] = my_var_label(0)
		misc_vars['user_out_var'] = user_out_var_label(0)
		misc_vars['d_floor'] = r'$\rho_{floor}$' + ' triggers'
		misc_vars['p_floor'] = r'$p_{floor}$' + ' triggers'
		misc_vars['dt_cs_r'] = r'$dt_{cs,r}$'
		misc_vars['dt_cs_t'] = r'$dt_{cs,\theta}$'
		misc_vars['dt_v_r'] = r'$dt_{v,r}$'
		misc_vars['dt_v_t'] = r'$dt_{v,\theta}$'
		misc_vars['dt_vA_r'] = r'$dt_{v_A,r}$'
		misc_vars['dt_vA_t'] = r'$dt_{v_A,\theta}$'


	# add all the labels to LabelDic
	LabelDic['hydro_keys'] = hydro_vars.keys()
	LabelDic['vorticity_keys'] = ['w_r','w_t','w_p','wp','w']
	LabelDic['misc_keys'] = misc_vars.keys()
	LabelDic['mhd_keys'] = mhd_vars.keys()
	for key in LabelDic['hydro_keys']:
		LabelDic[key] = hydro_vars[key]
	for key in LabelDic['misc_keys']:
		LabelDic[key] = misc_vars[key]
	for key in LabelDic['mhd_keys']:
		LabelDic[key] = mhd_vars[key]
	
	return LabelDic


def generate_grid_data(dump_dirs,i_dump=0,zoom_regions=None,R_zooms=None,rskip=1,tskip=1,ext='athdf'):
	""" Loads multiple simulation runs into a single dictionary
	
	Inputs:
	dump_dirs - paths to all sims (the output of dump_dir_dic())
	i_dump - integer specifying dump #

	Example usage:
	vars = ['d', 'v_r']
	SimDic = load_vars(i_dump,vars,dump_dirs,grid_dics)
	Sim['1']['d'] is an array of density for the 1st sim
	Sim['2']['v_r'] is an array of r-velocity for the 2nd sim
	etc...
	"""
	print "[generate_grid_data]:"

	all_keys = dump_dirs.keys()
	keys_to_use = []
	for key in all_keys:
		keys_to_use.append(key[0])
	keys_to_use = np.unique(keys_to_use)
	
	Nsims = len(keys_to_use)
	
	if zoom_regions is None:
		zoom_regions = [None,None]
	if R_zooms is None:
		R_zooms = [None,None]

	GridDics = {}
	
	for n,zoom_region,R_zoom in zip(range(Nsims),zoom_regions,R_zooms):
		
		idx = `n+1`
		
		# open the file using athena_read
		dump_file = dump_dirs[idx] + str(i_dump).zfill(5) + '.' + ext
		if ext == 'vtk':
			athena_dic = athena_read.vtk(dump_file)
		else:
			athena_dic = athena_read.athdf(dump_file)
		# r,theta = athena_dic[0],athena_dic[1]
		
		
		# Store zoom_region as indices
		grid_dic = {}
		if zoom_region is None:
			grid_dic['i_min'] = 0
			grid_dic['i_max'] = None
			grid_dic['j_min'] = 0
			grid_dic['j_max'] = None
			grid_dic['k_min'] = 0
			grid_dic['k_max'] = None
		else:
			grid_dic['i_min'] = zoom_region['i_min']
			grid_dic['i_max'] = zoom_region['i_max']
			grid_dic['j_min'] = zoom_region['j_min']
			grid_dic['j_max'] = zoom_region['j_max']

			if len(athena_dic['x3f']) > 2:
				grid_dic['k_min'] = zoom_region['k_min']
				grid_dic['k_max'] = zoom_region['k_max']
			else:
				grid_dic['k_min'] = 0
				grid_dic['k_max'] = None

		i_min = grid_dic['i_min']
		i_max = grid_dic['i_max']
		j_min = grid_dic['j_min']
		j_max = grid_dic['j_max']
		k_min = grid_dic['k_min']
		k_max = grid_dic['k_max']

		# load grid and calculate cell centered grid
		if i_max == None:
			r = athena_dic['x1f'][i_min:]/units.R
		else:
			r = athena_dic['x1f'][i_min:i_max+1]/units.R
		if j_max == None:
			theta = athena_dic['x2f'][j_min:]
		else:
			theta = athena_dic['x2f'][j_min:j_max+1]

		r_c = r[:-1] + 0.5 * np.ediff1d(r)
		theta_c = theta[:-1] + 0.5 * np.ediff1d(theta)
		grid_dic['r'], grid_dic['theta'] = r,theta
		grid_dic['r_c'], grid_dic['theta_c'] = r_c,theta_c
	
		# Determine zoom-in r-index, i_max, corresponding to R_zoom
		Nr = int(len(r_c)) 
		if R_zoom == None or R_zoom < r_c[0] or R_zoom > r_c[Nr-1]:
			print "\t [generate_grid_data]:"
			print "\t R_zoom is outside r-range = [{},{}]".format(r_c[0],r_c[Nr-1])
			print "\t Setting R_zoom to max(r)"
			i_max = Nr #Nr-1
			i_zoom = Nr-1
		else:
			R_diag = 1.1*np.sqrt(2.)*R_zoom
			dRs_cut = np.abs(r_c - R_diag)
			dRs_zoom = np.abs(r_c - R_zoom)
			i_max = min(Nr-1,dRs_cut.argmin())
			i_zoom = min(Nr-1,dRs_zoom.argmin())
		grid_dic['i_max'],grid_dic['i_zoom'] = i_max,i_zoom
		
		print "\tGenerating various grids for simulation {}...\n".format(n+1)
	
		# Generate Cartesian cell edge grid
		grid_dic['r_grid'], grid_dic['theta_grid'] = np.meshgrid(r[0:i_max+1],theta)
		grid_dic['x'] = grid_dic['r_grid']*np.sin(grid_dic['theta_grid'])
		grid_dic['y'] = grid_dic['r_grid']*np.cos(grid_dic['theta_grid'])

		# Generate Cartesian cell-centered grid
		r_c_grid, theta_c_grid = np.meshgrid(r_c[0:i_max],theta_c)
		sin_thetas, cos_thetas = np.sin(theta_c_grid),np.cos(theta_c_grid)
		grid_dic['r_c_grid'], grid_dic['theta_c_grid'] = r_c_grid, theta_c_grid
		grid_dic['x_c'], grid_dic['y_c']  = r_c_grid*sin_thetas, r_c_grid*cos_thetas
	
		# Save the grid of sin_thetas/cos_thetas 
		grid_dic['sin_thetas'], grid_dic['cos_thetas'] = sin_thetas, cos_thetas

		# Calculate index grids
		i_grid = np.zeros_like(r_c_grid)
		for j in range(len(theta_c)):
			i_grid[j,:] = j
			for i in range(len(r_c[0:i_max])):
				i_grid[j,i] = i
			
		j_grid = np.zeros_like(r_c_grid)
		for i in range(len(r_c[0:i_max])):
			j_grid[:,i] = i
			for j in range(len(theta_c)):
				j_grid[j,i] = j

		grid_dic['i_grid'], grid_dic['j_grid'] = i_grid, j_grid

		# Calculate sparse grids for displaying vector fields - 0 to j_min indices
		x_sparse = grid_dic['x_c'][0:j_min:tskip,i_min::rskip]
		y_sparse = grid_dic['y_c'][0:j_min:tskip,i_min::rskip]
		i_sparse = grid_dic['i_grid'][0:j_min:tskip,i_min::rskip]
		j_sparse = grid_dic['j_grid'][0:j_min:tskip,i_min::rskip]
		grid_dic['x_sparse0'],grid_dic['y_sparse0'] = x_sparse, y_sparse 
		grid_dic['i_sparse0'],grid_dic['j_sparse0'] = i_sparse, j_sparse 
	
		# Calculate sparse grids for displaying vector fields - j_min to j_max indices
		x_sparse = grid_dic['x_c'][j_min::tskip,i_min::rskip]
		y_sparse = grid_dic['y_c'][j_min::tskip,i_min::rskip]
		i_sparse = grid_dic['i_grid'][j_min::tskip,i_min::rskip]
		j_sparse = grid_dic['j_grid'][j_min::tskip,i_min::rskip]
		grid_dic['x_sparse'],grid_dic['y_sparse'] = x_sparse, y_sparse 
		grid_dic['i_sparse'],grid_dic['j_sparse'] = i_sparse, j_sparse 


		# Grid of cartesian cell sizes used for streamline/B-field line calc
		lengths=np.ones([len(theta_c),len(r_c)])
		#Loop over the r and theta arrays
		for j in range (len(theta_c)):
			for i in range(len(r_c)):
				if j>0:	
					dl1=np.abs(r_c[i]*np.cos(theta_c[j])-r_c[i]*np.cos(theta_c[j-1]))
				else:
					dl1=np.abs(r_c[i]*np.cos(theta_c[j])-r_c[i]*np.cos(theta_c[j+1]))
				if i>0:
					dl2=np.abs(r_c[i]-r_c[i-1])
				else:
					dl2=np.abs(r_c[i]-r_c[i+1])

				lengths[j][i]=np.min([dl1,dl2])
		grid_dic['lengths'] = lengths
		
		# store grid_dic in GridDics
		print "\tDone."
		print "\tGridDic[{}] has the following keys:\n{}".format(n+1,grid_dic.keys())
		GridDics[idx] = grid_dic
	
	return GridDics


def calc_grad_r(qty,r_c_grid):
	"""
	Take the gradient with respect to r.
	r_c_grid is the actual grid used in Athena,
	so we allow for a non-uniform mesh here.
	"""

	grad_qty = np.zeros_like(qty)

	#for j in range(qty.shape[0]):
	grad_qty[:,:-1] = (qty[:,1:]-qty[:,:-1])/(r_c_grid[:,1:]-r_c_grid[:,:-1])

	# now copy the gradients at r_out to the 
	# last r_c zone in qty so the shapes match 
	grad_qty[:,-1] = grad_qty[:,-2]

	return grad_qty

def calc_grad_t(qty,theta_c_grid):
	"""
	Take the gradient with respect to theta.
	theta_c_grid is the actual grid used in Athena,
	so we allow for a non-uniform mesh here.
	"""
	
	grad_qty = np.zeros_like(qty)

	grad_qty[:-1,:] = (qty[1:,:]-qty[:-1,:])/(theta_c_grid[1:,:]-theta_c_grid[:-1,:])

	# now copy the gradients at r_out to the 
	# last r_c zone in qty so the shapes match 
	grad_qty[-1,:] = grad_qty[-2,:]

	return grad_qty

def add_gradients(var_keys,sim_dics,grid_dics):
	
	Nsims = len(grid_dics.keys()) 
	
	for n in range(Nsims):
		
		idx = `n+1`
		
		# load grid for this sim
		grid_dic = grid_dics[idx]

		# we will add the gradients to sim_dic
		sim_dic = sim_dics[idx]

		for key in var_keys:
			grad_r_key = 'grad_' + key + '_r'
			grad_t_key = 'grad_' + key + '_t'
			sim_dic[grad_r_key] = calc_grad_r(sim_dic[key],grid_dic['r_c_grid'])
			sim_dic[grad_t_key] = calc_grad_t(sim_dic[key],grid_dic['theta_c_grid'])


def load_vars(i_dump,vars,dump_dirs,grid_dics,gamma=5./3.,MHD_check=0,ext='athdf',k=0):
	""" Loads multiple simulation runs into a single dictionary
	
	Inputs:
	i_dump - integer specifying dump # 
	vars - array of variables to collect
	dump_dirs - paths to all sims (the output of dump_dir_dic())
	grid_dics - grid data for all sims (the output of generate_grid_data())
	i_maxs - if set to True, the size of all output arrays will be limited
			 to i_max, which is contained in grid_dics
	gamma - adiabatic index

	Returns: A dictionary containing data of the variables listed in vars for
	each simulation specified in dump_dirs for dump i_dump

	Example usage:
	vars = ['d', 'v_r']
	SimDic = load_vars(i_dump,vars,dump_dirs,grid_dics)
	Sim['1']['d'] is an array of density for the 1st sim
	Sim['2']['v_r'] is an array of r-velocity for the 2nd sim
	etc...
	"""
	
	Nsims = len(grid_dics.keys()) 
	
	# load all keys
	label_dic = load_labels()
	
	VarDics = {}
	for n in range(Nsims):
		
		idx = `n+1`
		
		# load grid for this sim
		grid_dic = grid_dics[idx]
		
		# unpack region indices
		i_min = grid_dic['i_min']
		i_max = grid_dic['i_max']
		j_min = grid_dic['j_min']
		j_max = grid_dic['j_max']
		k_min = grid_dic['k_min']
		k_max = grid_dic['k_max']
		
		# open the dump file using athena_read
		dump_file = dump_dirs[idx] + str(i_dump).zfill(5) + '.' + ext
		if ext == 'vtk':
			athena_dic = athena_read.vtk(dump_file)
		else:
			athena_dic = athena_read.athdf(dump_file)
		
		# store output of each sim to 1 dictionary
		VarDics[idx] = {}
		
		# these are set to 0 if vars in the same group have been 
		# calculated to prevent duplicate computations
		group1, group2, group3, group4 = 1, 1, 1, 1
		
		# unpack primitive variables
		global d,p,v_r,v_t,v_p
		if ext == 'vtk':
			d = athena_dic[3]['rho'][k,j_min:j_max,i_min:i_max,0]*units.d
			p = athena_dic[3]['press'][k,j_min:j_max,i_min:i_max,0]
			v_r = athena_dic[3]['vel'][k,j_min:j_max,i_min:i_max,0]
			v_t = athena_dic[3]['vel'][k,j_min:j_max,i_min:i_max,1]
			v_p = athena_dic[3]['vel'][k,j_min:j_max,i_min:i_max,2]	
		else:
			d = athena_dic['rho'][k,j_min:j_max,i_min:i_max]*units.d
			p = athena_dic['press'][k,j_min:j_max,i_min:i_max]
			v_r = athena_dic['vel1'][k,j_min:j_max,i_min:i_max]
			v_t = athena_dic['vel2'][k,j_min:j_max,i_min:i_max]
			v_p = athena_dic['vel3'][k,j_min:j_max,i_min:i_max]	

		global MHDrun 
		try:
			B_r = athena_dic[3]['Bcc'][0,:,0:i_max,0]
			B_t = athena_dic[3]['Bcc'][0,:,0:i_max,1]
			B_p = athena_dic[3]['Bcc'][0,:,0:i_max,2]
			MHDrun = True	
		except:
			MHDrun = False	
		
		for var in vars:
			try:
				if var == 'd':
					qty = d
				elif var == 'p':
					qty = p
				elif var == 'v_r':
					qty = v_r
				elif var == 'v_t':
					qty = v_t
				elif var == 'v_p':
					v_kep = units.v_p*np.sqrt(1./(grid_dic['r_c_grid']*grid_dic['sin_thetas']))
					qty = v_p/v_kep
			
				# vorticity quantities
				elif var in label_dic['vorticity_keys']: 
					uov_key = idx + 'uov'
					if uov_key in dump_dirs.keys():
						uov_file = dump_dirs[uov_key] + str(i_dump).zfill(5) + '.vtk'
						print "[load_vars]: uov_file = {}".format(uov_file)
						uov_dic = athena_read.vtk(uov_file)
						w_r = uov_dic[3]['user_out_var0'][0,:,0:i_max]
						w_t = uov_dic[3]['user_out_var1'][0,:,0:i_max]
						w_p = uov_dic[3]['user_out_var2'][0,:,0:i_max]
						wp = np.sqrt(w_r**2 + w_t*2)
						w = np.sqrt(w_r**2 + w_t*2 + w_p**2)
						if var == 'w_r':
							qty = w_r
						elif var == 'w_t':
							qty = w_t
						elif var == 'w_p':
							qty = w_p
						elif var == 'wp':
							qty = wp
						elif var == 'w':
							qty = w
					else:
						print "Post-processing vorticity calculation not yet implemented!"
						sys.exit()
						
				# other derived hydro quantities				
				elif  var in label_dic['hydro_keys']: 
					global group1
# 					print "[SimDics::loadVars()]: group1 = {}, var = {}".format(group1,var)
					if group1:
						cs = units.cs*np.sqrt(gamma*p/d)
						vp = np.sqrt(v_r**2 + v_t**2)
						v = np.sqrt(v_r**2 + v_t**2 + v_p**2)
						group1 = 0
					
					if var == 'v': # poloidal velocity
						qty = v
					elif var == 'vp': # poloidal velocity
						qty = vp
					elif var == 'cs': # adiabatic sound speed
						qty = cs
					elif var == 'c_iso': # adiabatic sound speed
						qty = units.cs*np.sqrt(p/d)
					elif var == 's': # entropy
						qty = np.log(p/d**gamma)
					elif var == 'T': # temperature
						qty = units.T*p/d
						# filter = qty > 1e-20
						# qty[filter] = 1.
					elif var == 'T_vir': # virial temperature
						qty = 3.*grid_dic['r_c_grid']*p/d
					elif var == 'Mp': # poloidal Mach number
						qty = vp/cs	
					elif var == 'l_z': # magnitude of specific angular momentum
						qty = grid_dic['r_c_grid']*v_p*grid_dic['sin_thetas']
					elif var == 'l_p': # magnitude of specific angular momentum
						qty = grid_dic['r_c_grid']*v_t
					elif var == 'l': # magnitude of specific angular momentum
						qty = grid_dic['r_c_grid']*v_p*grid_dic['sin_thetas']
					elif var == 'Be': # Bernoulli function 
						qty = 0.5*v**2 + cs**2/(gamma - 1.) - \
						units.GM/grid_dic['r_c_grid'] 
					elif var == 'xi':  # photoionization parameter
						qty = units.xi/(d*grid_dic['r_c_grid']**2)	
					
				# MHD quantities	
				elif var in label_dic['mhd_keys']:
					global group2
		
					if group2:
						try:
							B = np.sqrt(B_r**2 + B_t**2 + B_p**2)
							Bp = np.sqrt(B_r**2 + B_t**2) # poloidal field
							v_A = Bp/np.sqrt(d)
							group2 = 0
						except:
							if MHD_check: 
								continue
							else:
								raise ValueError(var)
					if var == 'B_r':
						qty = B_r
					elif var == 'B_t':
						qty = B_t
					elif var == 'B_p':
						qty = B_p
					elif var == 'v_A': # Alven speed
						qty = v_A
					elif var == 'B': # magntude of B-field
						qty = B
					elif var == 'Bp':  # poloidal B-field
						qty = Bp
					elif var == 'beta': # plasma beta
						qty = 2.*p/B**2
					elif var == 'B2': # plasma beta
						qty = B**2
					elif var == 'Bphi_over_Bp': 
						qty = B_p/Bp

				# cell crossing times - e.g., dt_cs_t = r(dtheta)/|cs|
				elif var in ['dt_cs_r','dt_cs_t','dt_v_r','dt_v_t','dt_vA_r','dt_vA_t']:
					global group4
					if group4: # need all speeds
						cs = units.cs*np.sqrt(gamma*p/d)
						v = np.sqrt(v_r**2 + v_t**2 + v_p**2)

						dtheta_grid = grid_dic['theta_grid'][1:,:] - grid_dic['theta_grid'][0:-1,:] 
						rdtheta_grid = grid_dic['r_c_grid']*dtheta_grid[:,0:-1]
						dr_grid = grid_dic['r_grid'][:,1:] - grid_dic['r_grid'][:,0:-1]
						dr_grid = dr_grid[0:-1,:]

						group4 = 0

					if var == 'dt_cs_r': # sound crossing time in radial dir
						qty = dr_grid/cs
					elif var == 'dt_cs_t': # sound crossing time in polar dir
						qty = rdtheta_grid/cs
					if var == 'dt_v_r': # flow crossing time in radial dir
						qty = dr_grid/v
					elif var == 'dt_v_t': # flow crossing time in polar dir
						qty = rdtheta_grid/v
					elif var == 'dt_vA_r': # Alven crossing time in radial dir
						qty = dr_grid/v_A
					elif var == 'dt_vA_t': # Alven crossing time in polar dir
						qty = rdtheta_grid/v_A

				# misc quantities 
				elif var in ['d_floor','p_floor']:
					global group3
					if group3:
						T_vir = 3.*grid_dic['r_c_grid']*p/d
						max_filter = T_vir < 0.99e2
						min_filter = T_vir > 1.01e-3
						group3 = 0
					if var == 'd_floor': # map of zones hitting dens floor
						qty = np.copy(d)
						qty[max_filter] = 1e20 # np.nan
					elif var == 'p_floor': # map of zones hitting pres floor
						qty = np.copy(p)
						qty[min_filter] = 1e20 #np.nan
				
				# scratch variable		
				elif var == 'my_var':
					if MHDrun:
						qty = my_var(grid_dic,dump_dirs[idx],d,p,v_r,v_t,v_p,B_r,B_t,B_p)
					else:
						qty = my_var(grid_dic,dump_dirs[idx],d,p,v_r,v_t,v_p)
				
				# Athena++ user output scalar variable
# 				elif var.startswith('user_out'): #and not(uov_vector):
# 					# open the vtk file using athena_read
# 					uov_key = idx + 'uov'
# 					if uov_key in dump_dirs.keys():
# 						uov_file = dump_dirs[uov_key] + str(i_dump).zfill(5) + '.vtk'
# 						uov_dic = athena_read.vtk(uov_file)
# 						qty = uov_dic[3][var][0,:,0:i_max]
# 						qty = uov_scalar(qty,int(var[-1]))

				# Athena++ user output vector variable
				elif var.startswith('user_out'):
					print "\n\n USER_OUT_VAR!! \n\n"
					global VarDics
					index = int(var[-1])
					var_base_name = var[:-1]
					uovs = []

					# open the vtk file using athena_read
					uov_key = idx + 'uov'
					if uov_key in dump_dirs.keys():
						uov_file = dump_dirs[uov_key] + str(i_dump).zfill(5) + '.vtk'
						print "[load_vars]: uov_file = {}".format(uov_file)
						uov_dic = athena_read.vtk(uov_file)
						for n in range(index+1):
							uov_var = var_base_name + `n`
							print uov_var
							uovs.append(uov_dic[3][uov_var][0,:,0:i_max])
						if MHDrun:
							uovs = uov_vector(uovs,grid_dic,d,p,v_r,v_t,v_p,B_r,B_t,B_p)
						else:
							uovs = uov_vector(uovs,grid_dic,d,p,v_r,v_t,v_p)
						
						# if vector uov, add each component to VarDics
						if len(uovs) > 1:
							for n in range(index+1):
								uov_var = var_base_name + `n`
								VarDics[idx][uov_var] = uovs[n]
							qty = VarDics[idx][var] 
						else:
							qty = uovs[0]

				else: # variable not recognized - throw exception 
					raise ValueError(var)	
			
				VarDics[idx][var] = qty

			except ValueError as err:
				label_dic = load_labels()
# 				print "[loadvars()]: label_dic['mhd_keys'] = {}".format(label_dic['mhd_keys'])
				if var in label_dic['mhd_keys']:
					print "\n******\nERROR!\n******"
					print "MHD variable {} was recognized but not found for simulation:\n{}".format(err.args[0],dump_file)
					print "This must not be an MHD simulation."
					print "Choose from these hydro quantities:"
					print label_dic['hydro_keys']
				else:
					print "\n******\nERROR!\n******"
					print "Variable {} not recognized. Choose from:".format(err.args[0])
					print "hydro vars:\n{}".format(label_dic['hydro_keys'])
					print "mhd vars:\n{}".format(label_dic['mhd_keys'])
					print "misc vars:\n{}".format(label_dic['misc_keys'])
				sys.exit()

	
	return VarDics

