# Specify desired unit conversion from code units
import numpy as np
import athenaSetup_mp13 as inputs

""" Intermediate conversions (not used anywhere but here) """

# import athenaSetup_ZUnits as inputs; reload(inputs)

# # Specify any parameters that determine units
# eps = inputs.eps # disk scale height 

# # conversion to cgs units
# xi_i = inputs.xi_i

parsec = 3.0857e18		# kiloparsec in cm

""" Actual units used by load_vars() in SimDics """
d = inputs.d_units  # density units
v_r = 1.  # r-velocity units
v_t = 1.  # theta-velocity units
v_p = 1. #(inputs.v_esc/inputs.cs_b)*np.sqrt(inputs.r_in)  # phi-velocity units
p = 1.  # pressure/energy units
R = parsec/inputs.R_s  # distance units
GM = 0.5 #inputs.GM
T = 1. #inputs.T_units/inputs.T_C  # temperature units
cs = 1. 
xi = 1. #inputs.xi_disk #xi_i # photoionization parameter units