from pylab import *  # imports numpy as np and matplotlib
import array as ar
import sim_dics; reload(sim_dics)


""" LOAD DATA """
dump = 49  # output timestamp

# specify paths to the runs 
run_paths = {}
run_paths['1'] = ''

# specify output dump tags
dump_tags = {}
dump_tags['1'] = 'bondi.out2.'

# specify tags for user ouput variables 
uov_tags = {}
uov_tags = None

run_names = ['data_JHcontest2019']

# get sim paths
dump_dirs = sim_dics.dump_dir_dic(run_names=run_names,run_paths=run_paths,dump_tags=dump_tags)

# unpack grid data
GridDic = sim_dics.generate_grid_data(dump_dirs,dump)
r_c = GridDic['1']['r_c']
theta_c = GridDic['1']['theta_c']

vars = ['d','v_t','p','T']
SimDic = sim_dics.load_vars(dump,vars,dump_dirs,GridDic)


""" PLOTTING """
fig = figure(figsize=(8,8))

plot_func = semilogy
#############################
fig.add_subplot(111)

x_axis = theta_c*180./np.pi
plot_func(x_axis,SimDic['1']['d'][:,0],'m',ls='-',lw=2,label=r'$\rho$')

xlabel(r'$\theta$' + ' [deg]')
ylabel(r'$\rho(r_{in})$')
legend(loc='upper right')
title('Density along inner boundary')
#############################

show()